package asl.server;

import java.util.HashMap;
import java.util.Map;

import asl.logging.LogProvider;

/**
 * Provides each Thread its own Log to avoid synchronization lagging.
 * 
 * @author jost
 *
 */
public class ServerLogProvider extends LogProvider {

	private Map<Long, ServerLog> serverLogs = new HashMap<Long, ServerLog>();
	
	public ServerLog getLog() {

		long threadId = Thread.currentThread().getId();
		if (!serverLogs.containsKey(threadId)) {
			serverLogs.put(threadId, new ServerLog());
		}
		return serverLogs.get(threadId);
	}

	public Map<Long, ServerLog> getLogs() {
		return serverLogs;
	}
	
}
