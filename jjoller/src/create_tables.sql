DROP TABLE IF EXISTS queues, clients, messages, logs;

CREATE TABLE IF NOT EXISTS queues(
	id serial PRIMARY KEY,
	name VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS clients (
	id serial PRIMARY KEY,
	name VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS messages (
	id serial PRIMARY KEY,
	sender INT,
	receiver INT,
	arrival_time TIMESTAMP,
	queue int,
	message TEXT,
	FOREIGN KEY (queue) REFERENCES queues(id) ON DELETE CASCADE
);

/* Indexes */
CREATE INDEX inbox_index ON messages (queue,receiver,arrival_time);

/* Stored procedures */
CREATE OR REPLACE FUNCTION create_message(s INT, r INT, at TIMESTAMP, q INT, m TEXT) RETURNS void AS $$
  BEGIN
		INSERT INTO messages(sender, receiver, arrival_time, queue, message) VALUES(s,r,at,q,m);
  END;
  $$ LANGUAGE plpgsql;


/* Logs table */
CREATE TABLE IF NOT EXISTS logs (
	id serial PRIMARY KEY,
	machine_id INT,
	time TIMESTAMP,
	log TEXT,
	errors TEXT
);


