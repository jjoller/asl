package asl.logging;

import java.util.Map;

public abstract class LogProvider {

	public abstract Log getLog();

	public abstract Map<Long, ? extends Log> getLogs();

}
