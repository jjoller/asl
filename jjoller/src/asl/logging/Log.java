package asl.logging;

import java.sql.Timestamp;

import asl.Main;

/**
 * A log which can be stored in the database and retrieved by any component of
 * the system. The idea is that logs are stored in the database after the
 * termination of the tests in order to make collection of results easier.
 * 
 * @author jost
 *
 */
public class Log {

	public Log() {
		log = "";
		errors = "";
		this.machineId = Main.machineId;
		this.time = new Timestamp(System.currentTimeMillis());
	}

	private int machineId;
	protected String log;
	private String errors;
	private Timestamp time;

	public int getMachineId() {
		return machineId;
	}

	public void setMachineId(int machineId) {
		this.machineId = machineId;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	synchronized public void logError(Exception e) {
		this.errors += new Timestamp(System.currentTimeMillis()).toString()
				+ ": " + e.getClass().getName() + ", " + e.getMessage() + "\n";
	}

	@Override
	public String toString() {
		String s = "machine_id: " + machineId + ", time: " + time + "\n" + log
				+ "\nErrors:\n" + errors;
		return s;
	}
	
	/*
	 * not in the database
	 */
	protected long intervalStart;
	protected long loggingInterval = 1000;

	public long getLoggingInterval() {
		return loggingInterval;
	}

	synchronized public void setLoggingInterval(long loggingInterval) {
		this.loggingInterval = loggingInterval;
	}

}
