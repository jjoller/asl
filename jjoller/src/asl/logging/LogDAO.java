package asl.logging;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import asl.DAO;
import asl.DBConnectionPool;

/**
 * Saves and retrieves logs.
 * 
 * @author jost
 *
 */
public class LogDAO extends DAO {

	public LogDAO(DBConnectionPool connectionPool) throws SQLException {
		super(connectionPool);
	}

	/**
	 * Get a certain amount of logs. List the latest log first.
	 * 
	 * @param limit
	 *            The amount of logs to return.
	 * @return A list of logs.
	 */
	public List<Log> logs() {

		List<Log> logs = new ArrayList<Log>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = connectionPool.acquireConnection();
			pst = con.prepareStatement("SELECT * FROM logs ORDER BY time DESC");
			rs = pst.executeQuery();
			while (rs.next()) {
				logs.add(getLog(rs));
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);

		} finally {
			connectionPool.releaseConnection(con);
			try {
				if (rs != null) {
					rs.close();
				}
				if (pst != null) {
					pst.close();
				}

			} catch (SQLException ex) {
				log.log(Level.WARNING, ex.getMessage(), ex);
			}
		}
		return logs;
	}

	/**
	 * Save a log to the database.
	 * 
	 * @param aLog
	 */
	public void saveLog(Log aLog) {

		System.out.println("save log:\n" + aLog);

		PreparedStatement pst = null;
		Connection con = null;

		try {
			con = connectionPool.acquireConnection();
			String stm = "INSERT INTO logs(machine_id, time, log, errors) values(?,?,?,?)";
			pst = con.prepareStatement(stm);
			pst.setLong(1, aLog.getMachineId());
			pst.setTimestamp(2, aLog.getTime());
			pst.setString(3, aLog.getLog());
			pst.setString(4, aLog.getErrors());
			pst.executeUpdate();

		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			connectionPool.releaseConnection(con);
			try {
				if (pst != null) {
					pst.close();
				}

			} catch (SQLException ex) {
				log.log(Level.SEVERE, ex.getMessage(), ex);
			}
		}
	}

	/**
	 * Helper function to translate a database result object to a Log object.
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private Log getLog(ResultSet rs) throws SQLException {
		Log aLog = new Log();
		aLog.setMachineId(rs.getInt("machine_id"));
		aLog.setTime(rs.getTimestamp("time"));
		aLog.setLog(rs.getString("log"));
		aLog.setErrors(rs.getString("errors"));
		return aLog;
	}

}
