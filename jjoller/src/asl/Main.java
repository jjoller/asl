package asl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import asl.baseline.DummyConnectionPool;
import asl.client.ClientLog;
import asl.client.MessageClient;
import asl.client.request_factory.FirstFillDB;
import asl.client.request_factory.JustWrite;
import asl.client.request_factory.RequestFactory;
import asl.logging.Log;
import asl.logging.LogDAO;
import asl.message.MessagesOverview;
import asl.server.MessageServer;

/**
 * Root method. Runs either as server or as client depending on the input
 * parameters.
 * 
 * @author jost
 *
 */
public class Main {

	/*
	 * TODOs
	 * 
	 * x broadcasts x logging of the client x logging of exceptions x total busy
	 * time logging of server - db console - testing on dryad
	 */

	public static final int machineId = Math.abs(new Random().nextInt());

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, SQLException, InterruptedException {

		String mode = args[0];
		if (mode.equalsIgnoreCase("client")) {

			int numClients = new Integer(args[1]);
			String server = args[2];
			int messageLength = new Integer(args[3]);
			String dbServer = args[4];
			String workload = args[5];

			System.out.println("Start client, numClients: " + numClients
					+ " server: " + server + ", msgLength: " + messageLength
					+ ", dbServer: " + dbServer + ", workload: " + workload);

			// start clients

			List<Thread> threads = new ArrayList<Thread>();
			ClientLog theLog = new ClientLog();
			for (int i = 0; i < numClients; i++) {

				RequestFactory factory;
				if (workload.startsWith("randomized")) {
					String w = workload.replace("randomized", "");

					factory = new FirstFillDB(Integer.parseInt(w),
							messageLength);

				} else if (workload.startsWith("justwrite")) {

					factory = new JustWrite();

				} else {
					throw new IllegalStateException("Workload " + workload
							+ " not specified");
				}

				// Creating a client object
				MessageClient client = new MessageClient(theLog, server,
						MessageServer.PORT, factory);

				Thread thread = new Thread(client);
				threads.add(thread);
				thread.start();
			}

			// wait for termination
			for (Thread thread : threads) {
				thread.join();
			}

			// store the logs to the database
			LogDAO logDAO = new LogDAO(new DBConnectionPool(dbServer, 1));
			logDAO.saveLog(theLog);
			System.out.println("Client stored the log on the database: "
					+ dbServer);

		} else if (mode.equalsIgnoreCase("dbstatus")) {
			// get a brief overview of the messages table
			// java -jar git/asl/asl_jjoller/ASL.jar dbstatus 54.171.43.147
			String dbServer = args[1];
			DBConnectionPool pool = new DBConnectionPool(dbServer, 1);
			MessagesOverview overview = new MessagesOverview(new DAO(pool));
			System.out.println(overview);

		} else if (mode.equalsIgnoreCase("dbreset")) {
			// delete the database and set up tables
			// java -jar ASL.jar dbreset 54.77.255.6
			new DAO(new DBConnectionPool(args[1], 1)).resetDatabase();

		} else if (mode.equalsIgnoreCase("logs")) {
			// read logs from the database
			// java -jar ASL.jar logs 54.77.255.6

			String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss")
					.format(Calendar.getInstance().getTime());
			File logFile = new File("logs/" + timeLog + "-" + args[2] + ".csv");
			BufferedWriter writer = new BufferedWriter(new FileWriter(logFile));

			List<Log> logs = new LogDAO(new DBConnectionPool(args[1], 1))
					.logs();
			if (logs.size() == 0) {
				System.out.println("There are no logs on the database.");
			} else {
				Log server = null;
				Log client = null;
				for (Log l : logs) {
					String e = l.toString();
					if (e.contains("DELETE_QUEUE")) {
						// is server log
						server = l;
					} else {
						// is client log
						client = l;
					}
				}
				if (server != null && client != null) {
					String[] serverLines = server.toString().split("\n");
					String[] clientLines = client.toString().split("\n");
					String out = serverLines[1] + "," + clientLines[1] + "\n";
					for (int i = 4; i < Math.min(serverLines.length,
							clientLines.length) - 2; i++) {
						out += serverLines[i] + "," + clientLines[i] + "\n";
					}
					writer.write(out);
				} else {
					writer.write("server or client log is null "
							+ (server != null) + " - " + (client != null));
				}

			}
			// Close writer
			writer.close();

		} else if (mode.equalsIgnoreCase("server")) {
			// run as server
			// java -jar ASL.jar server 54.77.255.6 1 60
			String dbServer = args[1];
			int numThreads = new Integer(args[2]);
			int numConnections = new Integer(args[3]);
			int runningTimeSeconds = new Integer(args[4]);
			System.out.println("Start server with dbserver " + dbServer
					+ " and " + numThreads + " threads, db connections: "
					+ numConnections + " which runs " + runningTimeSeconds
					+ " seconds.");

			DBConnectionPool pool = new DBConnectionPool(dbServer,
					numConnections);
			DAO messageDAO = new DAO(pool);
			LogDAO logDAO = new LogDAO(pool);

			new Thread(new MessageServer(messageDAO, logDAO, numThreads,
					runningTimeSeconds)).start();

			System.out.println("Server running...");
		} else if (mode.equalsIgnoreCase("mergecsv")) {
			int variable = Integer.parseInt(args[1]);
			File folder = new File(".");
			String outfile = "out.csv";
			File out = new File(outfile);
			boolean headerWritten = false;
			try (FileOutputStream fos = new FileOutputStream(out);
					BufferedWriter bw = new BufferedWriter(
							new OutputStreamWriter(fos));) {
				File[] files = folder.listFiles();
				for (File file : files) {
					if (file.getName().endsWith(".csv")
							&& !file.getName().equals(outfile)) {
						System.out.println("filename: " + file.getName());
						String[] splits = file.getName().split("-");
						String var = splits[variable];
						try (FileInputStream fstream = new FileInputStream(file);
								BufferedReader br = new BufferedReader(
										new InputStreamReader(fstream));) {
							String strLine;
							// Read File Line By Line
							String head = br.readLine();
							if (head != null && !headerWritten) {
								bw.write(head + ",variable\n");
								headerWritten = true;
							}
							while ((strLine = br.readLine()) != null) {
								// Print the content on the console
								strLine += "," + var;
								bw.write(strLine + "\n");
							}
						}
					}
				}
			}
		} else if (mode.equals("dbbaseline")) {

			String dbServer = args[1];
			int numThreads = new Integer(args[2]);
			int numConnections = new Integer(args[3]);
			DBConnectionPool pool = new DBConnectionPool(dbServer,
					numConnections);
			new DBBaselineBenchmark(new DAO(pool), numThreads);
		} else if (mode.equals("mwbaseline")) {

			// run as server
			// java -jar ASL.jar server 54.77.255.6 1 60
			String dbServer = args[1];
			int numThreads = new Integer(args[2]);
			int numConnections = new Integer(args[3]);
			int runningTimeSeconds = new Integer(args[4]);
			System.out.println("Start server with dbserver " + dbServer
					+ " and " + numThreads + " threads, db connections: "
					+ numConnections + " which runs " + runningTimeSeconds
					+ " seconds.");


			DBConnectionPool pool = new DummyConnectionPool(dbServer,
					numConnections);
			DAO messageDAO = new DAO(pool);
			LogDAO logDAO = new LogDAO(pool);

			new Thread(new MessageServer(messageDAO, logDAO, numThreads,
					runningTimeSeconds)).start();

			System.out.println("Server running...");
		} else {
			System.out
					.println("possible execution modes are client, server, dbstatus, dbreset and logs");
		}
	}
}
