package asl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import asl.message.ErrorMessages;
import asl.message.Message;

/**
 * How to set up mongodb: 1. on db server: open port 5432 2. on db server:
 * /etc/postgresql/9.3/main/pg_hba.conf add line "host all all 0.0.0.0/0 md5" 3.
 * on db server: Open postgresql.conf, set listen_addresses to '*'
 */
public class DAO {

	protected static final Logger log = Logger.getLogger(DAO.class.getName());

	// psql -h localhost -d asl -U postgres
	public static final String DATABASE_NAME = "asl";
	public static final String DATABASE_USER = "postgres";
	public static final String DATABASE_PASSWORD = "asl2014";

	public DAO(DBConnectionPool connectionPool) throws SQLException {
		this.connectionPool = connectionPool;
	}

	// idea for indexes
	// B-Tree index on arrival_time
	// Hash indices on sender, receiver and queue Ids

	protected DBConnectionPool connectionPool;

	public DBConnectionPool getConnectionPool() {
		return connectionPool;
	}

	/**
	 * Write this message to the database. Automatically generate an id.
	 * 
	 * @param message
	 * @throws DBException
	 */
	public void saveMessage(Message message) throws DBException {

		// String stm =
		// "INSERT INTO messages(sender, receiver, arrival_time, queue, message) values(?,?,?,?,?);";

		// call the stored procedure to store a message
		String stm = "SELECT create_message(?,?,?,?,?);";
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			pst.setInt(1, message.getSender());
			pst.setInt(2, message.getReceiver());
			pst.setTimestamp(3, message.getArrivalTime());
			pst.setInt(4, message.getQueue());
			pst.setString(5, message.getMessage());
			pst.executeQuery();

		} catch (SQLException ex) {
			if (ex.getMessage().contains("violates foreign key constraint")) {
				throw new DBException(ErrorMessages.QUEUE_NOT_EXIST.getMsg());
			}
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
	}

	/**
	 * Get the queues in which there is a message for the given client.
	 * 
	 * @param client
	 * @return
	 */
	public Set<Integer> inboxQueues(int client) {

		Set<Integer> result = new HashSet<Integer>();

		String stm = "SELECT DISTINCT queue FROM messages WHERE receiver = ? OR receiver = 0;";

		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			pst.setInt(1, client);
			ResultSet resultSet = pst.executeQuery();
			while (resultSet.next()) {
				result.add(resultSet.getInt("queue"));
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return result;
	}

	/**
	 * Get last message from a particular sender for a particular receiver
	 * 
	 * @param sender
	 * @return
	 */
	public Message messageFromSender(int sender, int receiver) {

		String stm = "SELECT * FROM messages WHERE sender = ? AND ( receiver = ? OR receiver = 0 ) ORDER BY arrival_time ASC LIMIT 1;";
		Message message = null;
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			pst.setInt(1, sender);
			pst.setInt(2, receiver);
			ResultSet resultSet = pst.executeQuery();
			if (resultSet.next()) {
				message = getMessage(resultSet);
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return message;
	}

	/**
	 * Read the oldest message from a queue which is addressed to the given
	 * receiver.
	 * 
	 * @param queue
	 * @param receiver
	 * @param delete
	 * @return
	 */
	public Message topQueue(int queue, int receiver) {

		String stm = "SELECT * FROM messages WHERE queue = ? AND ( receiver = ? OR receiver = 0 ) ORDER BY arrival_time ASC LIMIT 1;";
		// String stm = "SELECT top_queue(?,?);";
		Message message = null;
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			pst.setInt(1, queue);
			pst.setInt(2, receiver);
			ResultSet resultSet = pst.executeQuery();
			if (resultSet.next()) {
				message = getMessage(resultSet);
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return message;
	}

	public Message popQueue(int queue, int receiver) {

		String stm = "DELETE FROM messages WHERE id IN ( SELECT id FROM messages WHERE queue = ? AND ( receiver = ? OR receiver = 0 ) ORDER BY arrival_time ASC LIMIT 1 ) RETURNING *;";
		// String stm = "SELECT top_queue(?,?);";
		Message message = null;
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			pst.setInt(1, queue);
			pst.setInt(2, receiver);
			ResultSet resultSet = pst.executeQuery();
			if (resultSet.next()) {
				message = getMessage(resultSet);
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return message;

	}

	/**
	 * Delete message.
	 * 
	 * @param message
	 */
	public Message deleteMessage(Message message) {
		if (message.getId() == 0) {
			throw new IllegalStateException("Message id is 0");
		}

		Message msg = null;
		String stm = "DELETE FROM messages WHERE id = ? RETURNING *;";
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			pst.setInt(1, message.getId());
			// pst.execute();
			ResultSet set = pst.executeQuery();
			if (set.next()) {
				msg = this.getMessage(set);
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return msg;
	}

	/**
	 * Return true if the client exists
	 * 
	 * @param clientId
	 * @return
	 */
	public boolean clientExists(int clientId) {

		String stm = "SELECT * FROM clients WHERE id = ?;";

		boolean result = false;
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			pst.setInt(1, clientId);
			result = pst.executeQuery().next();
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return result;
	}

	/**
	 * Load and return all messages which are currently in the database.
	 * 
	 * @return
	 */
	public List<Message> allMessages() {

		String stm = "SELECT * FROM messages";
		List<Message> messages = new ArrayList<Message>();
		ResultSet rs = null;
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			rs = pst.executeQuery();
			while (rs.next()) {
				messages.add(getMessage(rs));
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}

		return messages;
	}

	/**
	 * Number of messages in the database
	 * 
	 * @return
	 */
	public int numMessages() {

		String stm = "SELECT COUNT(*) FROM messages;";
		ResultSet rs = null;
		int num = 0;
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			rs = pst.executeQuery();
			if (rs.next()) {
				num = rs.getInt(1);
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}

		return num;
	}

	/**
	 * Number of messages in a certain queue
	 * 
	 * @return
	 */
	public int numMessages(int queue) {

		String stm = "SELECT COUNT(*) FROM messages WHERE queue = ?;";

		ResultSet rs = null;
		int num = 0;
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			pst.setInt(1, queue);
			rs = pst.executeQuery();
			if (rs.next()) {
				num = rs.getInt(1);
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}

		return num;
	}

	/**
	 * Distinct clients that sent at least one message.
	 * 
	 * @return
	 */
	public Set<Integer> senders() {

		String stm = "SELECT DISTINCT sender FROM messages;";

		ResultSet rs = null;
		Set<Integer> senders = new HashSet<Integer>();
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			rs = pst.executeQuery();
			while (rs.next()) {
				senders.add(rs.getInt("sender"));
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return senders;
	}

	/**
	 * Distinct clients that received at least one message.
	 * 
	 * @return
	 */
	public Set<Integer> receivers() {

		String stm = "SELECT DISTINCT receiver FROM messages;";
		ResultSet rs = null;
		Set<Integer> receivers = new HashSet<Integer>();
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			rs = pst.executeQuery();
			while (rs.next()) {
				receivers.add(rs.getInt("receiver"));
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return receivers;
	}

	/**
	 * Translate a result set into a message.
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private Message getMessage(ResultSet rs) throws SQLException {

		Message message = new Message();
		message.setId(rs.getInt(1));
		message.setSender(rs.getInt(2));
		message.setReceiver(rs.getInt(3));
		message.setArrivalTime(rs.getTimestamp(4));
		message.setQueue(rs.getInt(5));
		message.setMessage(rs.getString(6));

		return message;
	}

	/**
	 * Number of messages in the database
	 * 
	 * @return
	 */
	public Set<Integer> queues() {

		String stm = "SELECT * FROM queues;";

		ResultSet rs = null;
		Set<Integer> queues = new HashSet<Integer>();
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			rs = pst.executeQuery();
			while (rs.next()) {
				queues.add(rs.getInt("id"));
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return queues;
	}

	public Set<Integer> clients() {

		String stm = "SELECT * FROM clients;";

		ResultSet rs = null;
		Set<Integer> queues = new HashSet<Integer>();
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			rs = pst.executeQuery();
			while (rs.next()) {
				queues.add(rs.getInt("id"));
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return queues;
	}

	/**
	 * Return true if there is a message in the given queue. A queue only exists
	 * if there is a message in it.
	 * 
	 * @param queue
	 * @return
	 */
	public boolean queueExists(int queue) {

		String stm = "SELECT * FROM queues WHERE id = ? LIMIT 1;";

		boolean result = false;
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			pst.setInt(1, queue);
			result = pst.executeQuery().next();
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return result;
	}

	/**
	 * Delete all messages of a certain queue
	 * 
	 * @param queue
	 */
	public void deleteQueue(int queue) {

		String stm = "DELETE FROM queues WHERE id = ? ;";
		Connection con = this.connectionPool.acquireConnection();
		try (PreparedStatement pst = con.prepareStatement(stm)) {
			pst.setInt(1, queue);
			pst.execute();
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
	}

	public int createQueue() {
		int queueId = -1;
		Connection con = this.connectionPool.acquireConnection();
		String stmt = "INSERT INTO queues(name) values(?) ;";
		try (PreparedStatement pst = con.prepareStatement(stmt,
				Statement.RETURN_GENERATED_KEYS)) {
			pst.setString(1, "queue");
			pst.executeUpdate();
			// Using the getGeneratedKeys() method to retrieve
			// the key(s). In this case there is only one key column
			ResultSet keyResultSet = pst.getGeneratedKeys();
			if (keyResultSet.next()) {
				queueId = keyResultSet.getInt(1);
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return queueId;
	}

	public int createClient() {
		int clientId = -1;
		Connection con = this.connectionPool.acquireConnection();
		String stmt = "INSERT INTO clients(name) values(?) ;";
		try (PreparedStatement pst = con.prepareStatement(stmt,
				Statement.RETURN_GENERATED_KEYS)) {
			pst.setString(1, "queue");
			pst.executeUpdate();
			// Using the getGeneratedKeys() method to retrieve
			// the key(s). In this case there is only one key column
			ResultSet keyResultSet = pst.getGeneratedKeys();
			if (keyResultSet.next()) {
				clientId = keyResultSet.getInt(1);
			}
		} catch (SQLException ex) {
			log.log(Level.SEVERE, ex.getMessage(), ex);
		} finally {
			this.connectionPool.releaseConnection(con);
		}
		return clientId;
	}

	public void resetDatabase() {

		try {

			InputStream is = DAO.class.getClassLoader().getResourceAsStream(
					"create_tables.sql");
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));
			StringBuilder out = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line + "\n");
			}
			String stmt = out.toString();
			System.out.println("Reset table: \n" + stmt);

			Connection con = this.connectionPool.acquireConnection();
			try (PreparedStatement pst = con.prepareStatement(stmt)) {
				pst.execute();
				log.info("Database resetted");
			} catch (SQLException ex) {
				log.log(Level.SEVERE, ex.getMessage(), ex);
			} finally {
				this.connectionPool.releaseConnection(con);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
