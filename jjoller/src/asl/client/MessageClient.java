package asl.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.logging.Logger;

import asl.client.request_factory.RequestFactory;
import asl.message.ErrorMessages;
import asl.message.Request;
import asl.message.Response;

/**
 * 
 * A client of the server.
 * 
 * @author jost
 *
 */
public class MessageClient implements Runnable {

	private static final Logger log = Logger.getLogger(MessageClient.class
			.getName());

	public MessageClient(ClientLog theLog, String serverAddress, int port,
			RequestFactory requestFactory) throws SQLException {
		this.serverAddress = serverAddress;
		this.port = port;
		this.requestFactory = requestFactory;
		this.theLog = theLog;
	}

	private String serverAddress;
	private int port;
	private RequestFactory requestFactory;
	private ClientLog theLog;

	@Override
	public void run() {

		long t = System.currentTimeMillis();

		Request req = requestFactory.firstRequest();
		boolean terminated = false;
		while (req != null && !terminated) {

			Socket socket = null;
			ObjectOutputStream os = null;
			InputStream input = null;
			OutputStream output = null;
			ObjectInputStream ois = null;

			try {
				long clientThinkTime, waitingForConnectionTime, upstreamTime, waitingForResponseTime;

				// log.info("Client Request:\n" + req.getOperation()
				// + ", sender: " + req.getSender() + ", receiver: "
				// + req.getReceiver());
				clientThinkTime = System.currentTimeMillis() - t;

				/*
				 * Establish connection to server
				 */
				t = System.currentTimeMillis();
				socket = new Socket(serverAddress, port);
				waitingForConnectionTime = System.currentTimeMillis() - t;

				/*
				 * Send request to server
				 */
				t = System.currentTimeMillis();
				input = socket.getInputStream();
				output = socket.getOutputStream();
				os = new ObjectOutputStream(output);
				if (req.getOperation() == null) {
					throw new IllegalStateException();
				}
				os.writeObject(req);
				os.flush();
				upstreamTime = System.currentTimeMillis() - t;

				/*
				 * Receive response from server
				 */
				t = System.currentTimeMillis();
				ois = new ObjectInputStream(input);
				Response resp = (Response) ois.readObject();
				waitingForResponseTime = System.currentTimeMillis() - t;

				/*
				 * Client think time: computes the next request and does some
				 * logging
				 */
				t = System.currentTimeMillis();
				theLog.logRequest(clientThinkTime, waitingForConnectionTime,
						upstreamTime, waitingForResponseTime);

				// client waits a bit
				Thread.sleep(0);
				
				// log.info("Server Response:\n" + resp);
				if (resp.getError() != null
						&& resp.getError().equals(
								ErrorMessages.SERVER_TERMINATED.getMsg())) {
					terminated = true;
					log.info("Ther server sent a termination message, do not send any more requests.");
				} else {
					req = requestFactory.nextRequest(resp);
				}
				
				
			} catch (IOException | ClassNotFoundException
					| InterruptedException e) {
				e.printStackTrace();
				theLog.logError(e);
			} finally {
				// close everything which can be closed
				try {
					if (socket != null) {
						socket.close();
					}
					if (os != null) {
						os.close();
					}
					if (input != null) {
						input.close();
					}
					if (output != null) {
						output.close();
					}
					if (ois != null) {
						ois.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}