package asl.client;

import asl.logging.Log;
import asl.logging.RunningStat;

public class ClientLog extends Log {

	public ClientLog() {
		super();
		reset();
		writeCSVHeader();
	}

	private RunningStat clientThinkTime = new RunningStat();
	private RunningStat waitingForConnectionTime = new RunningStat();
	private RunningStat upstreamTime = new RunningStat();
	private RunningStat waitingForResponseTime = new RunningStat();

	private long totalRequests, failedRequests;

	synchronized public void logError(Exception e) {
		super.logError(e);
		totalRequests++;
		failedRequests++;
	}

	synchronized public void logRequest(long clientThinkTime, long waitingForConnectionTime,
			long upstreamTime, long waitingForResponseTime) {

		totalRequests++;
		this.clientThinkTime.push(clientThinkTime);
		this.waitingForConnectionTime.push(waitingForConnectionTime);
		this.upstreamTime.push(upstreamTime);
		this.waitingForResponseTime.push(waitingForResponseTime);

		if ((System.currentTimeMillis() - intervalStart) > loggingInterval) {
			writeCSVLog();
			reset();
		}
	}

	private void reset() {

		totalRequests = 0;
		failedRequests = 0;
		clientThinkTime = new RunningStat();
		waitingForConnectionTime = new RunningStat();
		upstreamTime = new RunningStat();
		waitingForResponseTime = new RunningStat();
		intervalStart = System.currentTimeMillis();
	}

	private void writeCSVLog() {

		double seconds = ((double) this.loggingInterval) / 1000.0;
		log += System.currentTimeMillis();
		log += "," + ((double) totalRequests) / seconds;
		log += "," + ((double) failedRequests) / seconds;

		log += "," + clientThinkTime.mean();
		log += "," + clientThinkTime.variance();

		log += "," + waitingForConnectionTime.mean();
		log += "," + waitingForConnectionTime.variance();

		log += "," + upstreamTime.mean();
		log += "," + upstreamTime.variance();

		log += "," + waitingForResponseTime.mean();
		log += "," + waitingForResponseTime.variance();

		log += "\n";
	}

	private void writeCSVHeader() {
		this.log = this.log
				+ "timeStamp,totalRequests,failedRequests,clientThinkTime,clientThinkTimeVar,waitingForConnectionTime,waitingForConnectionTimeVar,upstreamTime,upstreamTimeVar,waitingForResponseTime,waitingForResponseTimeVar\n";
	}

}
