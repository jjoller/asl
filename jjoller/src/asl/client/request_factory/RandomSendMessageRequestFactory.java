package asl.client.request_factory;

import java.util.Random;

import asl.RandomSeed;
import asl.Utils;
import asl.message.Operation;
import asl.message.Request;
import asl.message.Response;

/**
 * Creates random 'send message' requests.
 * 
 * @author jost
 *
 */
public class RandomSendMessageRequestFactory implements RequestFactory {

	public RandomSendMessageRequestFactory(int userId, int maxUsers,
			int maxQueues, int messageLength) {
		this.userId = userId;
		this.maxQueues = maxQueues;
		this.maxUsers = maxUsers;
		this.messageLength = messageLength;
	}

	private int userId;
	private int maxQueues;
	private int maxUsers;
	private int messageLength;
	private Random random = new Random(RandomSeed.getSeed());

	@Override
	public Request nextRequest(Response resp) {

		Request req = new Request();
		req.setOperation(Operation.SEND_MESSAGE);
		req.setSender(userId);

		// With the same probability do either a broadcast or send to a specific
		// client.
		if (random.nextBoolean()) {
			req.setReceiver(random.nextInt(maxUsers) + 1);
		} else {
			// broadcast
			req.setReceiver(0);
		}

		req.setQueue(random.nextInt(maxQueues) + 1);
		req.setMessage(Utils.loremIpsum(messageLength));
		return req;
	}

	@Override
	public Request firstRequest() {
		// TODO Auto-generated method stub
		return null;
	}

}
