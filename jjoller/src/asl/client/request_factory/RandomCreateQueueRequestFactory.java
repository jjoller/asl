package asl.client.request_factory;

import java.util.Random;

import asl.RandomSeed;
import asl.message.Operation;
import asl.message.Request;
import asl.message.Response;

/**
 * Create random 'Create Queue' requests
 * 
 * @author jost
 *
 */
public class RandomCreateQueueRequestFactory implements RequestFactory {

	public RandomCreateQueueRequestFactory(int maxQueues) {
		this.maxQueues = maxQueues;
	}

	private int maxQueues;
	private Random random = new Random(RandomSeed.getSeed());

	@Override
	public Request nextRequest(Response resp) {

		Request req = new Request();

		req.setOperation(Operation.CREATE_QUEUE);
		req.setQueue(random.nextInt(maxQueues) + 1);
		
		return req;
	}

	@Override
	public Request firstRequest() {
		// TODO Auto-generated method stub
		return null;
	}

}
