package asl.client.request_factory;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import asl.RandomSeed;
import asl.Utils;
import asl.message.Operation;
import asl.message.Request;
import asl.message.Response;

/**
 * Payload which first fills the db until there are a certain amount of messages
 * in the db. After the db is filled up, the client keeps the balance of sending
 * vs popping messages.
 * 
 * @author jost
 *
 */
public class FirstFillDB implements RequestFactory {

	static final Logger log = Logger.getLogger(FirstFillDB.class.getName());

	public FirstFillDB() {
		this(100000, 200);
	}

	public FirstFillDB(int messageLoad, int messageLength) {
		this.messageLoad = messageLoad;
		this.messageLength = messageLength;
	}

	final int numQueues = 50;
	final int messageLength;
	final int messageLoad;
	int clientId;
	Request lastRequest;
	int numMessagesSent = 0;
	int balance = 0;
	boolean fillMode = true;
	int numQueuesCreated = 0;
	Set<Integer> clients = new HashSet<Integer>();
	Set<Integer> queues = new HashSet<Integer>();

	@Override
	public Request nextRequest(Response response) {

		if (lastRequest.getOperation() == Operation.QUEUE_LIST) {

			if (response.getQueues() != null) {
				for (int q : response.getQueues()) {
					this.queues.add(q);
				}
			}
			if (queues.size() < numQueues) {
				Request req = new Request();
				req.setSender(this.clientId);
				req.setOperation(Operation.CREATE_QUEUE);
				return req(req);
			}
		} else if (lastRequest.getOperation() == Operation.CREATE_QUEUE) {
			queues.add(response.getQueue());
			Request req = new Request();
			if (this.clients.size() < 1) {
				req.setOperation(Operation.CLIENT_LIST);
			} else {
				req.setSender(this.clientId);
				req.setMessage(Utils.loremIpsum(this.messageLength));
				req.setReceiver(Utils.getRandom(clients));
				req.setQueue(response.getQueue());
				req.setOperation(Operation.SEND_MESSAGE);
			}
			return req(req);
		} else if (lastRequest.getOperation() == Operation.CREATE_CLIENT) {
			clientId = response.getSender();
			return req(this.fillDB());
		} else if (lastRequest.getOperation() == Operation.MESSAGE_COUNT) {
			int count = Integer.parseInt(response.getMessage());
			if (count >= messageLoad - 10) {
				fillMode = false;
			} else {
				return req(fillDB());
			}
		} else if (lastRequest.getOperation() == Operation.INBOX_QUEUES) {

			int[] queues = response.getQueues();
			if (queues.length > 0) {

				for (int q : queues) {
					this.queues.add(q);
				}
				Request req = new Request();
				req.setSender(this.clientId);
				if (RandomSeed.random.nextBoolean()) {
					req.setOperation(Operation.POP_QUEUE);
					balance--;
				} else {
					req.setOperation(Operation.TOP_QUEUE);
				}
				return req(req);
			}
		} else if (lastRequest.getOperation() == Operation.CLIENT_LIST) {
			for (int q : response.getQueues()) {
				if (q != this.clientId) {
					this.clients.add(q);
				}
			}
			return req(fillDB());
		}

		if (fillMode) {
			if (numMessagesSent % 9 == 0) {
				Request req = new Request();
				req.setOperation(Operation.MESSAGE_COUNT);
				return req(req);
			} else if (numMessagesSent % 10 == 0) {
				Request req = new Request();
				req.setOperation(Operation.CLIENT_LIST);
				return req(req);
			}
			return req(fillDB());
		} else {
			return req(payLoadRequest());
		}
	}

	private Request req(Request req) {
		this.lastRequest = req;
		if (req.getOperation() == Operation.SEND_MESSAGE) {
			this.numMessagesSent++;
		}
		return req;
	}

	@Override
	public Request firstRequest() {
		Request req = new Request();
		req.setOperation(Operation.CREATE_CLIENT);
		lastRequest = req;
		return req;
	}

	private Request fillDB() {

		Request request = new Request();
		request.setSender(this.clientId);

		if (this.queues.size() < 50) {
			request.setOperation(Operation.QUEUE_LIST);
			return request;
		}

		if (this.clients.size() < 1) {
			request.setOperation(Operation.CLIENT_LIST);
			return request;
		}

		request.setReceiver(Utils.getRandom(clients));
		request.setMessage(Utils.loremIpsum(this.messageLength));
		request.setQueue(Utils.getRandom(queues));
		request.setOperation(Operation.SEND_MESSAGE);
		return request;
	}

	private Request payLoadRequest() {

		Request req = new Request();
		// if (this.balance > 10) {
		if (RandomSeed.random.nextBoolean()) {
			// top or pop queue, but first get inbox queues
			req.setOperation(Operation.INBOX_QUEUES);
			req.setReceiver(this.clientId);
			req.setSender(this.clientId);
		} else {
			if (clients.size() < 1) {
				req.setOperation(Operation.CLIENT_LIST);
			} else {
				// send message
				req.setOperation(Operation.SEND_MESSAGE);
				req.setSender(this.clientId);
				req.setReceiver(Utils.getRandom(clients));
				req.setMessage(Utils.loremIpsum(this.messageLength));
				this.balance++;
			}
		}
		return req;
	}
}
