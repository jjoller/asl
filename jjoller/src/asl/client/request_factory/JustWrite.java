package asl.client.request_factory;

import java.util.Random;

import asl.RandomSeed;
import asl.Utils;
import asl.message.Operation;
import asl.message.Request;
import asl.message.Response;

/**
 * Workload which just writes messages
 * 
 * @author jost
 *
 */
public class JustWrite implements RequestFactory {

	int queue = -1;
	Random random = new Random(RandomSeed.getSeed());

	@Override
	public Request nextRequest(Response response) {

		if (response.getQueue() > 0) {
			queue = response.getQueue();
		}

		Request req = new Request();
		req.setSender(random.nextInt(10));
		if (queue < 0) {
			req.setOperation(Operation.CREATE_QUEUE);
		} else {
			req.setOperation(Operation.SEND_MESSAGE);
			req.setReceiver(0);
			req.setQueue(queue);
			req.setMessage(Utils.loremIpsum(200));
		}
		return req;
	}

	@Override
	public Request firstRequest() {
		Request req = new Request();
		req.setOperation(Operation.CREATE_CLIENT);
		return req;
	}

}
