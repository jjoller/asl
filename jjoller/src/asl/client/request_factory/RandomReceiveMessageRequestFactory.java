package asl.client.request_factory;

import java.util.Random;

import asl.RandomSeed;
import asl.message.Operation;
import asl.message.Request;
import asl.message.Response;

public class RandomReceiveMessageRequestFactory implements RequestFactory {

	public RandomReceiveMessageRequestFactory(int userId,int maxQueues){
		this.userId = userId;
		this.maxQueues = maxQueues;
	}
	
	private int userId;
	private int maxQueues;
	private Random random = new Random(RandomSeed.getSeed());

	
	@Override
	public Request nextRequest(Response resp) {
		
		Request req = new Request();
		req.setOperation(Operation.RECEIVE_MESSAGE);
		req.setReceiver(userId);
		req.setQueue(random.nextInt(maxQueues) + 1);
		return req;
	}


	@Override
	public Request firstRequest() {
		// TODO Auto-generated method stub
		return null;
	}

}
