package asl.client.request_factory;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import asl.RandomSeed;
import asl.message.Operation;
import asl.message.Request;
import asl.message.Response;

public class RandomRequestFactory implements RequestFactory {

	public RandomRequestFactory(int userId, int maxUsers, int messageLength,
			int maxQueues) {

		this(userId, maxUsers, messageLength, maxQueues, EnumSet
				.allOf(Operation.class));
	}

	public RandomRequestFactory(int userId, int maxUsers, int messageLength,
			int maxQueues, Set<Operation> operations) {

		this.userId = userId;
		this.maxUsers = maxUsers;
		this.messageLength = messageLength;
		this.maxQueues = maxQueues;
		operations.remove(Operation.CREATE_CLIENT);
		operations.remove(Operation.DELETE_QUEUE);
		operations.remove(Operation.CLIENT_LIST);
		operations.remove(Operation.MESSAGE_COUNT);
		operations.remove(Operation.QUEUE_LIST);

		this.operations = new Operation[operations.size()];
		int i = 0;
		for (Operation o : operations) {
			this.operations[i++] = o;
		}
	}

	protected Map<Operation, RequestFactory> factories = null;
	protected Random random = new Random(RandomSeed.getSeed());
	protected int userId, maxUsers, messageLength, maxQueues;
	protected Operation[] operations;

	@Override
	public Request nextRequest(Response resp) {

		if (factories == null) {
			factories = this.factories();
		}

		// choose a random operation among the available operations and return
		// the corresponding request factory.
		Operation op = operations[random.nextInt(operations.length)];
		System.out.println(op);
		RequestFactory rf = factories.get(op);

		return rf.nextRequest(resp);
	}

	protected Map<Operation, RequestFactory> factories() {

		Map<Operation, RequestFactory> factories;

		factories = new HashMap<Operation, RequestFactory>();
		factories.put(Operation.CREATE_QUEUE,
				new RandomCreateQueueRequestFactory(maxQueues));
		factories.put(Operation.INBOX_QUEUES,
				new RandomInboxQueuesRequestFactory(userId));
		factories.put(Operation.POP_QUEUE, new RandomPopQueueRequestFactory(
				userId, maxQueues));
		factories.put(Operation.TOP_QUEUE, new RandomTopQueueRequestFactory(
				userId, maxQueues));
		factories.put(Operation.RECEIVE_MESSAGE,
				new RandomReceiveMessageRequestFactory(userId, maxQueues));
		factories.put(Operation.SEND_MESSAGE,
				new RandomSendMessageRequestFactory(userId, maxUsers,
						maxQueues, messageLength));
		return factories;
	}

	@Override
	public Request firstRequest() {

		Request req = new Request();
		req.setOperation(Operation.CREATE_CLIENT);
		req.setSender(userId);
		return req;
	}
}