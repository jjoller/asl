package asl.client.request_factory;

import asl.message.Request;
import asl.message.Response;

/**
 * Factory for creating client requests.
 * 
 * @author jost
 *
 */
public interface RequestFactory {
	
	Request nextRequest(Response response);

	Request firstRequest();
}
