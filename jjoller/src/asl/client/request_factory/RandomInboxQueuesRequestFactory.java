package asl.client.request_factory;

import asl.message.Operation;
import asl.message.Request;
import asl.message.Response;

public class RandomInboxQueuesRequestFactory implements RequestFactory {

	public RandomInboxQueuesRequestFactory(int userId){
		this.userId = userId;
	}
	
	private int userId;
	
	@Override
	public Request nextRequest(Response resp) {
		
		Request req = new Request();
		req.setOperation(Operation.INBOX_QUEUES);
		req.setReceiver(userId);
		return req;
	}

	@Override
	public Request firstRequest() {
		// TODO Auto-generated method stub
		return null;
	}

}
