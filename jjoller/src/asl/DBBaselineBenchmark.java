package asl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import asl.message.Message;

public class DBBaselineBenchmark {

	DBBaselineBenchmark(DAO dao, int clients) throws InterruptedException {

		Count count = new Count();
		count.start = System.currentTimeMillis();
		for (int i = 0; i < clients; i++) {
			Thread t = new Thread(new Job(dao, count));
			t.start();
			threads.add(t);
		}

		for (Thread t : threads) {
			t.join();
		}
		double seconds = ((double) (count.end - count.start)) / 1000.0;
		System.out.println(((double) count.n) / seconds);
		System.out.println("failures: " + count.failures);
	}

	List<Thread> threads = new ArrayList<Thread>();

}

class Count {

	int n = 0;
	int failures = 0;
	long start, end;

	synchronized void incr(int n, int failures) {
		end = System.currentTimeMillis();
		this.n += n;
		this.failures += failures;
	}

}

class Job implements Runnable {

	public Job(DAO dao, Count count) {
		this.dao = dao;
		this.count = count;
	}

	Random random = new Random(RandomSeed.getSeed());
	private DAO dao;
	private Count count;

	public void run() {

		List<Integer> queues = new ArrayList<Integer>();

		for (int i = 0; i < 10; i++) {
			queues.add(dao.createQueue());
		}

		Message message = new Message();
		message.setMessage(Utils.loremIpsum(10));
		long start = System.currentTimeMillis();
		int n = 0;
		int failures = 0;
		for (int i = 0; (System.currentTimeMillis() - start) < 10000; i++) {
			try {
				writeReadMessage(message, queues.get(i % queues.size()),
						random.nextInt(100));
				n += 2;
			} catch (DBException e) {
				e.printStackTrace();
				failures++;
			}
		}
		count.incr(n, failures);
	}

	private void writeReadMessage(Message message, int queue, int sender)
			throws DBException {
		message.setQueue(queue);
		message.setSender(sender);
		message.setReceiver(0);
		dao.saveMessage(message);
		dao.popQueue(queue, 0);
	}
}
