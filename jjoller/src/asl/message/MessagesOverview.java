package asl.message;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import asl.DAO;

public class MessagesOverview {

	public MessagesOverview(DAO dao) throws SQLException {
		

		Set<Integer> senders = dao.senders();
		Set<Integer> receivers = dao.receivers();

		this.numSenders = senders.size();
		this.numReceivers = senders.size();

		Set<Integer> clients = new HashSet<Integer>();
		clients.addAll(senders);
		clients.addAll(receivers);

		this.numClients = clients.size();
		Set<Integer> queues = dao.queues();
		this.numQueues = queues.size();
		this.numMessages = dao.numMessages();

		messagesPerQueue = new HashMap<Integer, Integer>();

		for (int q : queues) {
			messagesPerQueue.put(q, dao.numMessages(q));
		}

	}

	private int numClients;
	private int numSenders;
	private int numReceivers;
	private int numQueues;
	private int numMessages;
	private Map<Integer, Integer> messagesPerQueue;

	public String toString() {
		
		String s = "#clients: " + numClients + ", senders: " + numSenders
				+ ", receivers: " + numReceivers + "\n#queues: " + numQueues
				+ "\n#messages: " + numMessages;

		for (Entry<Integer, Integer> e : messagesPerQueue.entrySet()) {
			s += "\n\tqueue: " + e.getKey() + " contains " + e.getValue()
					+ " messages";
		}

		return s;
	}
}
