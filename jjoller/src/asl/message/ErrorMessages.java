package asl.message;

public enum ErrorMessages {

	QUEUE_NOT_EXIST("Queue does not exist"), NO_SUCH_MESSAGE(
			"There is no message"), QUEUE_ALREADY_EXISTS(
			"The queue already exists."), ILLEGAL_QUEUE_IDENTIFIER(
			"Is not a valid queue identifier."), UNSPECIFIED_OPERATION(
			"Unspecified Operation"), SERVER_TERMINATED("Server terminated");

	private ErrorMessages(String msg) {
		this.msg = msg;
	}

	private String msg;

	public String getMsg() {
		return msg;
	}
}
