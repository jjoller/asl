package asl.message;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import asl.Utils;

/**
 * Generates messages.
 * 
 * @author jost
 *
 */
public class MessageFactory {
	
	/**
	 * Generate messages with given parameters
	 * 
	 * @param amount
	 *            The amount of messages to generate.
	 * @param messageLength
	 *            The length of the message string.
	 * @param sender
	 *            The sender.
	 * @param receiver
	 *            The receiver.
	 * @param queue
	 *            The queue.
	 * @return
	 */
	public List<Message> testMessages(int amount, int messageLength,
			int sender, int receiver, int queue) {

		List<Message> messages = new ArrayList<Message>();
		Message msg = new Message();
		msg.setQueue(queue);
		long t = System.currentTimeMillis();
		for (int i = 0; i < amount; i++) {

			Message message = new Message();
			message.setSender(sender);
			message.setReceiver(receiver);
			message.setQueue(queue);
			message.setArrivalTime(new Timestamp(t++));
			message.setMessage(Utils.loremIpsum(messageLength));
			messages.add(message);
		}

		// postcondition
		if (messages.size() != amount) {
			throw new IllegalStateException();
		}
		return messages;
	}

}
