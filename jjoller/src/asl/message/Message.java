package asl.message;

import java.io.Serializable;
import java.sql.Timestamp;

public class Message implements Serializable {

	private static final long serialVersionUID = -5106987626983937526L;

	public Message() {

	}

	public Message(Message req) {
		this.sender = req.getSender();
		this.receiver = req.getReceiver();
		this.arrivalTime = req.getArrivalTime();
		this.queue = req.getQueue();
		this.message = req.getMessage();
	}

	private int id;
	private int sender;
	private int receiver;
	private Timestamp arrivalTime;
	private int queue;
	private String message;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSender() {
		return sender;
	}

	public void setSender(int sender) {
		this.sender = sender;
	}

	public int getReceiver() {
		return receiver;
	}

	public void setReceiver(int receiver) {
		this.receiver = receiver;
	}

	public Timestamp getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Timestamp arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getQueue() {
		return queue;
	}

	public void setQueue(int queue) {
		this.queue = queue;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		String s = "";
		s += "sender: " + sender + "\n";
		s += "receiver: " + receiver + "\n";
		s += "arrivalTime: " + arrivalTime + "\n";
		s += "queue: " + queue + "\n";
		s += "message: " + message;
		return s;
	}

}
