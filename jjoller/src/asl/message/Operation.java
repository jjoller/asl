package asl.message;

import java.util.EnumSet;

public enum Operation {

	CREATE_CLIENT, CLIENT_LIST, QUEUE_LIST, MESSAGE_COUNT, CREATE_QUEUE, DELETE_QUEUE, SEND_MESSAGE, RECEIVE_MESSAGE, POP_QUEUE, TOP_QUEUE, INBOX_QUEUES;

	// Clients can create and delete queues
	// Clients can send and receive messages
	// Clients can read a queue by either removing the topmost message or just
	// by looking at its contents
	// Clients can send a message to a queue indicating a particular receiver
	// If a message has an explicit receiver, it can only be accessed by that
	// receiver (but there is no need to implement a client authentication
	// system)
	// Clients can query for messages from a particular sender (at most one
	// message is returned)
	// Clients can query for queues where messages for them are waiting

	public static EnumSet<Operation> fromString(String operations) {

		operations = operations.trim();
		String[] splits = operations.split(",");
		EnumSet<Operation> result = EnumSet.noneOf(Operation.class);
		for (String split : splits) {
			split = split.trim();
			Operation o = Operation.valueOf(split);
			if (o != null) {
				result.add(o);
			}
		}
		return result;
	}

}
