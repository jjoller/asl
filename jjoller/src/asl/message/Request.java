package asl.message;

public class Request extends Message {

	private static final long serialVersionUID = -5567408832816210393L;

	private Operation operation;

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	@Override
	public String toString() {
		String s = operation + "\n";
		s += super.toString();
		return s;
	}

}
