package asl.message;

public class Response extends Message {

	public Response() {
		// default constructor
	}

	public Response(Message msg) {
		super(msg);
	}

	private static final long serialVersionUID = -3208694271556745954L;
	private String error;
	private int[] queues;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int[] getQueues() {
		return queues;
	}

	public void setQueues(int[] queues) {
		this.queues = queues;
	}

	@Override
	public String toString() {
		String s = "";
		if (error != null) {
			s += "ERROR: " + error + "\n";
		}
		if (queues != null) {
			s += "Queues: \n";
			for (long queue : queues) {
				s += queue + "\n";
			}
		}
		return s + super.toString();
	}

}