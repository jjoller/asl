package asl.simulation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MMnQueue implements Component {

	public MMnQueue(double serviceTime, int m, String name) {
		this(serviceTime, m, -1, name);
	}

	public MMnQueue(double serviceTime, int m, int capacity, String name) {
		this.serviceTime = serviceTime;
		if (capacity > 0) {
			queue = new LinkedBlockingQueue<Job>(capacity);
		} else {
			queue = new LinkedBlockingQueue<Job>();
		}
		this.m = m;
		this.name = name;
	}

	protected int m;
	//protected double decay = 0;
	protected double serviceTime;
	protected BlockingQueue<Job> queue;
	protected Component sink;
	protected String name;
//	private int jobCount = 0;
//	private long t = System.currentTimeMillis();
	private List<Worker> workers = new ArrayList<Worker>();

	public void setSink(Component sink) {
		this.sink = sink;
		for (int i = 0; i < m; i++) {
			Worker w = new Worker();
			workers.add(w);
			new Thread(w).start();
		}
	}

	public void stop() {
		for (Worker w : workers) {
			w.stop();
		}
	}

	@Override
	public void enqueue(Job job) {
		try {
		//	job.tic();
			queue.put(job);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

/*
	public String toString() {
		return name.toUpperCase() + " throughput: " + this.throughput;
	}
*/
	
	protected void doJob(){
		Job job = queue.poll();
		if (job != null) {
			try {
				Thread.sleep((long) (1000 * serviceTime));
				sink.enqueue(job);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	class Worker implements Runnable {

		/*
		public Worker(MMnQueue queue) {
			this.queue = queue;
		}
*/
	//	private MMnQueue queue;
		private boolean isStopped = false;

		public void stop() {
			isStopped = true;
		}

		@Override
		public void run() {
			while (!isStopped) {
				doJob();
			}
		}
	}
}
