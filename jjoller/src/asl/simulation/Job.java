package asl.simulation;

public class Job {

	public Job() {

	}

	private long time;
	private long firstTic = 0;
	private long lastToc = 0;
	private int completions = 0;
	private double decay = 0.9;
	private double avgResponseTime = 0;

	public void tic() {
		time = System.currentTimeMillis();
		if (firstTic == 0) {
			firstTic = time;
		}
	}

	public void toc() {
		long t = System.currentTimeMillis();
		if (time > 0) {
			lastToc = t;
			double timeVanished = t - time;
			if (time == 0) {
				avgResponseTime = timeVanished;
			} else {
				avgResponseTime = avgResponseTime * decay + timeVanished
						* (1 - decay);
			}
			completions++;
		}
	}

	public double getThroughput() {
		return ((double) completions * 1000) / (double) (lastToc - firstTic);
	}

	public double getAvgResponseTime() {
		return avgResponseTime / 1000.0;
	}

}
