package asl.simulation;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class BlockingMMnQueue extends MMnQueue {

	public BlockingMMnQueue(double serviceTime, int m, int capacity, String name) {

		super(serviceTime, m, capacity, name);
		workers = new LinkedBlockingQueue<Worker>();
		try {
			for (int i = 0; i < m; i++) {
				workers.put(new Worker());
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		executor = new ThreadPoolExecutor(m, m, 100, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>());
	}

	protected BlockingQueue<Worker> workers;
	private ThreadPoolExecutor executor;

	public void setSink(Component sink) {
		this.sink = sink;
	}

	public void stop() {
		executor.shutdown();
	}

	@Override
	public void enqueue(Job job) {

		try {
			Worker worker = workers.take();
			// worker.setJob(job);
			executor.execute(worker);
			worker.block.take();
			sink.enqueue(job);

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (RejectedExecutionException e) {
			// ignore
		}
	}

	class Worker implements Runnable {

		// public Worker(BlockingMMnQueue q){
		// this.q=q;
		// }

		// private Job job;
		// public void setJob(Job job){
		// this.job = job;
		// }
		// BlockingMMnQueue q;
		private BlockingQueue<Object> block = new LinkedBlockingQueue<Object>();

		@Override
		public void run() {
			try {
				Thread.sleep((long) (1000 * serviceTime));
				workers.put(this);
				block.put(new Object());
				// q.sink.enqueue(job);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}