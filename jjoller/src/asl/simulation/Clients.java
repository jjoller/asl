package asl.simulation;

import java.util.ArrayList;
import java.util.List;

public class Clients extends MMnQueue {

	/**
	 * 
	 * @param sink
	 * @param thinkTime
	 *            Time to think before sending the next request in seconds
	 */

	public Clients(double serviceTime, int m, double warmupTime) {
		super(serviceTime, m, -1, "Clients");
		this.warmupTime = warmupTime * 1000;
	}

	// private int jobCount;
	private long startTime = System.currentTimeMillis();
	private double warmupTime;
	// private long t;
	// private double decay = 0.9;
	// private double avgResponseTime, throughput;
	// private Object lock = new Object();

	private List<Job> jobs = new ArrayList<Job>();

	public void start() {
		for (int i = 0; i < m; i++) {
			Job job = new Job();
			jobs.add(job);
			enqueue(job);
		}
	}

	@Override
	public void enqueue(Job job) {

		// update throughput
		if (System.currentTimeMillis() - startTime > warmupTime) {
			job.toc();
		}
		try {
			this.queue.put(job);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}
	
	@Override
	protected void doJob(){
		Job job = queue.poll();
		if (job != null) {
			try {
				Thread.sleep((long) (1000 * serviceTime));
				job.tic();
				sink.enqueue(job);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public double getAvgResponseTime() {

		double avgResponseTime = 0;
		for (Job job : jobs) {
			avgResponseTime += job.getAvgResponseTime();
		}
		return avgResponseTime / jobs.size();
	}

	public double getThroughput() {

		double tp = 0;
		for (Job job : jobs) {
			tp += job.getThroughput();
		}
		return tp;
	}

	public String toString() {
		return "Response Time: " + getAvgResponseTime() + ", Throughput: "
				+ getThroughput();
	}

}
