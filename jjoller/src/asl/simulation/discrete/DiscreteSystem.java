package asl.simulation.discrete;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class DiscreteSystem extends Simulation {

	public DiscreteSystem(int numClients, int clientThinkTime, int timeSteps,
			int stepsPerSecond, int mwServers, int dbServers,
			int mwServiceTime, int dbServiceTime) {
		super(numClients, clientThinkTime, timeSteps, stepsPerSecond);

		this.mwServers = mwServers;
		this.dbServers = dbServers;
		this.mwServiceTime = mwServiceTime;
		this.dbServiceTime = dbServiceTime;

	}

	int mwServers;
	int dbServers;
	int mwServiceTime;
	int dbServiceTime;

	List<Job> jobs = new ArrayList<Job>();
	Queue<Job> client = new LinkedList<Job>();
	Queue<Job> mwQueue = new LinkedList<Job>();
	Queue<Job> mwService = new LinkedList<Job>();
	Queue<Job> dbQueue = new LinkedList<Job>();
	Queue<Job> dbService = new LinkedList<Job>();

	public String toString() {
		return mwServers + "," + jobs.size() + "," + getAvgResponseTime() + ","
				+ getThroughput();
	}

	public void step(int currentTime) {

		// move job from client to mw queue
		while (client.size() > 0) {
			Job job = client.peek();
			if (currentTime - job.entryTime >= clientThinkTime) {
				client.remove(job);
				mwQueue.add(job);
				job.leaveClientTime = currentTime;
			} else {
				break;
			}
		}

		// move job from mw queue to mw
		while (mwService.size() < mwServers && mwQueue.size() > 0) {
			Job job = mwQueue.poll();
			mwService.add(job);
			job.entryTime = currentTime;
		}

		// move job from mw to dbQueue
		for (Job job : mwService) {
			if (currentTime - job.entryTime >= mwServiceTime && !job.isInDB) {
				dbQueue.add(job);
				job.isInDB = true;
			}
		}

		// move job from db queue to db
		while (dbService.size() < dbServers && dbQueue.size() > 0) {
			Job job = dbQueue.poll();
			dbService.add(job);
			job.entryTime = currentTime;
		}

		// move job from db to client
		List<Job> toRemove = new ArrayList<Job>();
		for (Job job : dbService) {
			if (currentTime - job.entryTime >= dbServiceTime) {
				client.add(job);
				job.entryTime = currentTime;
				job.completions++;
				job.totalTimeInSystem += currentTime - job.leaveClientTime;
				job.isInDB = false;
				toRemove.add(job);
			}
		}
		dbService.removeAll(toRemove);
		mwService.removeAll(toRemove);
		toRemove.clear();

	}

	public String header() {
		return "threads,clients,waitingForResponseTime,totalRequests\n";
	}

}
