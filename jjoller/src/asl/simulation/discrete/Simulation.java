package asl.simulation.discrete;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public abstract class Simulation {

	public Simulation(int numClients, int clientThinkTime, int timeSteps,
			int stepsPerSecond) {

		this.clientThinkTime = clientThinkTime;
		this.timeSteps = timeSteps;
		this.stepsPerSecond = stepsPerSecond;

		for (int i = 0; i < numClients; i++) {
			Job job = new Job();
			jobs.add(job);
			client.add(job);
			job.entryTime = 0;
		}
		this.timeSteps = timeSteps;
		this.stepsPerSecond = stepsPerSecond;

	}

	public static void main(String[] args) throws IOException {
		//run1QueueSimSmall();
		run1QueueSimLarge();
		//runDbXSim();
	}

	public static void run1QueueSimSmall() throws IOException {

		List<Simulation> dbThrouputSimulations = new ArrayList<Simulation>();

		int[] clients = { 1, 2, 3, 4, 5, 6, 7, 8 };
		int[] servers = { 1, 2, 4, 8 };

		/*
		 * int mwServers = 10; int dbServers = 20; int clientThinkTime = 30; int
		 * mwServiceTime = 0; int dbServiceTime = 104;
		 */
		int timeSteps = 1000;
		int stepsPerSecond = 1000;
		int clientThinkTime = 2;
		int serviceTime = 11;

		for (int c : clients) {
			for (int s : servers) {
				// int numClients, int clientThinkTime, int timeSteps, int
				// stepsPerSecond, int m, int serviceTime
				Simulation sim = new ClosedSystemMMNQueue(c, clientThinkTime,
						timeSteps, stepsPerSecond, s,3, serviceTime);
				sim.run();
				dbThrouputSimulations.add(sim);
			}
		}
		Simulation.writeCSV("1QueueModelSmall", dbThrouputSimulations);

	}
	
	public static void run1QueueSimLarge() throws IOException {

		List<Simulation> dbThrouputSimulations = new ArrayList<Simulation>();

		int[] clients = { 1, 2, 4, 8,12,16,20,24,28,32,36,40,44,50 };
		int[] servers = { 50 };

		/*
		 * int mwServers = 10; int dbServers = 20; int clientThinkTime = 30; int
		 * mwServiceTime = 0; int dbServiceTime = 104;
		 */
		int timeSteps = 10000;
		int stepsPerSecond = 10000;
		int clientThinkTime = 20;
		int serviceTime = 105;

		for (int c : clients) {
			for (int s : servers) {
				// int numClients, int clientThinkTime, int timeSteps, int
				// stepsPerSecond, int m, int serviceTime
				Simulation sim = new ClosedSystemMMNQueue(c, clientThinkTime,
						timeSteps, stepsPerSecond, s,12, serviceTime);
				sim.run();
				dbThrouputSimulations.add(sim);
			}
		}
		Simulation.writeCSV("1QueueModelLarge", dbThrouputSimulations);

	}

	public static void runDbXSim() throws IOException {
		List<Simulation> dbThrouputSimulations = new ArrayList<Simulation>();

		int[] clients = { 2, 4, 8, 12, 16, 20, 24, 32, 28, 36, 40, 44, 50 };

		int mwServers = 50;
		int dbServers = 12;
		int clientThinkTime = 30;
		int mwServiceTime = 0;
		int dbServiceTime = 104;

		/*
		 * int mwServers = 10; int dbServers = 20; int clientThinkTime = 30; int
		 * mwServiceTime = 0; int dbServiceTime = 104;
		 */
		int timeSteps = 10000;
		int stepsPerSecond = 10000;

		for (int c : clients) {
			Simulation s = new DiscreteSystem(c, mwServers, dbServers,
					clientThinkTime, mwServiceTime, dbServiceTime, timeSteps,
					stepsPerSecond);
			s.run();
			dbThrouputSimulations.add(s);
		}
		Simulation.writeCSV("mwThroughput", dbThrouputSimulations);

	}

	public static void writeCSV(String name, List<Simulation> confs)
			throws IOException {

		String header = confs.get(0).header();
		File file = new File(name + "Sim.csv");
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(header);

		for (int i = 0; i < confs.size(); i++) {

			Simulation conf = confs.get(i);

			bw.write(conf.toString() + "\n");

		}
		bw.close();
	}

	int timeSteps;
	int stepsPerSecond;
	int clientThinkTime;

	List<Job> jobs = new ArrayList<Job>();
	Queue<Job> client = new LinkedList<Job>();

	abstract void step(int time);

	public void run() {
		for (int i = 0; i < timeSteps; i++) {
			step(i);
			step(i);
			step(i);
			step(i);
			step(i);
			step(i);
		}
	}

	public String header() {
		return "clients,waitingForResponseTime,totalRequests\n";
	}

	public String toString() {
		return this.client.size() + "," + this.getAvgResponseTime() + ","
				+ this.getThroughput();
	}

	/**
	 * Get throughput in jobs per seconds.
	 * 
	 * @return
	 */
	double getThroughput() {
		int completions = 0;
		for (Job job : jobs) {
			completions += job.completions;
		}
		return (completions * stepsPerSecond) / (double) timeSteps;
	}

	/**
	 * Get average response time in milliseconds
	 * 
	 * @return
	 */
	double getAvgResponseTime() {
		double responseTime = 0;
		double completions = 1;
		for (Job job : jobs) {

			responseTime += job.totalTimeInSystem;
			completions += job.completions;
		}

		System.out.println("R: " + responseTime + " X: " + completions);

		return (responseTime * 1000.0) / (completions * stepsPerSecond);
	}

}
