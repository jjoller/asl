package asl.simulation.discrete;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ClosedSystemMMNQueue extends Simulation {

	public ClosedSystemMMNQueue(int numClients, int clientThinkTime,
			int timeSteps, int stepsPerSecond, int m,int maxN, int serviceTime) {
		super(numClients, clientThinkTime, timeSteps, stepsPerSecond);
		this.m = m;
		this.maxN = maxN;
		this.serviceTime = serviceTime;
	}

	private int m, maxN, serviceTime;
	private Queue<Job> queue = new LinkedList<Job>();
	private Queue<Job> server = new LinkedList<Job>();

	public void step(int currentTime) {

		// move job from client to queue
		while (client.size() > 0) {
			Job job = client.peek();
			if (currentTime - job.entryTime >= clientThinkTime) {
				client.remove(job);
				queue.add(job);
				job.leaveClientTime = currentTime;
			} else {
				break;
			}
		}

		// move job from queue to server
		while (server.size() < m && server.size() < maxN && queue.size() > 0) {
			Job job = queue.poll();
			server.add(job);
			job.entryTime = currentTime;
		}

		// move job from db to client
		List<Job> toRemove = new ArrayList<Job>();
		for (Job job : server) {
			if (currentTime - job.entryTime >= serviceTime) {
				client.add(job);
				job.entryTime = currentTime;
				job.completions++;
				job.totalTimeInSystem += currentTime - job.leaveClientTime;
				toRemove.add(job);
			}
		}
		server.removeAll(toRemove);
	}

	public String header() {
		return "threads,clients,waitingForResponseTime,totalRequests\n";
	}

	public String toString() {
		return this.m + "," + this.jobs.size() + ","
				+ this.getAvgResponseTime() + "," + this.getThroughput();
	}
}
