package asl.simulation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RunSimulation {

	public static void main(String[] args) throws InterruptedException,
			IOException {

	//	List<Configuration> confs = dbThroughputConfigurations();

		List<Configuration> confs = mwThroughputConfigurations();
		
		List<Clients> results = testSystem(confs);

//		writeCSV("dbThroughput", confs, results);
		writeCSV("mwThroughput", confs, results);

		
		for (int i = 0; i < results.size(); i++) {
			System.out.println(results.get(i) + " -- " + confs.get(i));
		}

	}

	public static List<Configuration> dbThroughputConfigurations() {

		List<Configuration> confs = new ArrayList<Configuration>();
		for (int threads = 1; threads <= 8; threads *= 2) {
			for (int clients = 1; clients <= 8; clients++) {
				confs.add(new Configuration(threads, clients));
			}
		}
		return confs;
	}
	
	public static List<Configuration> mwThroughputConfigurations(){
		
		List<Configuration> confs = new ArrayList<Configuration>();
		int[] clients = {2,4,8,12,16,20,24,32,28,36,40,44,50};
		for(int c:clients){
			confs.add(new DBLConfiguration(8, c));
		}
		return confs;
	}

	public static void writeCSV(String name, List<Configuration> confs,
			List<Clients> results) throws IOException {

		String header = "threads,clients,waitingForResponseTime,totalRequests\n";
		File file = new File(name + "Sim.csv");
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(header);

		for (int i = 0; i < confs.size(); i++) {
			Configuration conf = confs.get(i);
			Clients res = results.get(i);

			bw.write(conf.middleWareWorkers + "," + conf.numClients + ","
					+ res.getAvgResponseTime() * 1000.0 + ","
					+ res.getThroughput() + "\n");

		}
		bw.close();
	}

	public static List<Clients> testSystem(List<Configuration> configurations)
			throws InterruptedException {

		List<Clients> results = new ArrayList<Clients>();
		for (Configuration c : configurations) {
			results.add(simulateSystemExperiment(c));
		}
		return results;
	}

	public static Clients simulateSystemExperiment(Configuration c)
			throws InterruptedException {

		// MMnQueue clients = new MMnQueue(0, numClients, "client");
		MMnQueue middleWares = new MMnQueue(c.middleWareServiceTime,
				c.middleWareWorkers, "middleWare");
		MMnQueue db = new BlockingMMnQueue(c.dbServiceTime, c.dbWorkers, -1,
				"db");

		Clients clients = new Clients(c.clientThinkTime, c.numClients, 0.5);

		// clients.setSink(middleWares);
		clients.setSink(middleWares);
		middleWares.setSink(db);
		db.setSink(clients);

		clients.start();
		Thread.sleep((long) (2.2 * 1000));

		clients.stop();
		middleWares.stop();
		db.stop();
		
		Thread.sleep((long) (0.1 * 1000));

		return clients;
	}

}

class Configuration {

	public Configuration(int middleWareWorkers, int numClients) {
		this.middleWareWorkers = middleWareWorkers;
		this.numClients = numClients;
	}

	public Configuration(int numClients, int middleWareWorkers, int dbWorkers,
			double middleWareServiceTime, double dbServiceTime,
			int dbQueueCapacity, double clientThinkTime) {

		this.numClients = numClients;
		this.middleWareWorkers = middleWareWorkers;
		this.dbWorkers = dbWorkers;
		this.middleWareServiceTime = middleWareServiceTime;
		this.dbServiceTime = dbServiceTime;
		this.dbQueueCapacity = dbQueueCapacity;
		this.clientThinkTime = clientThinkTime;
	}

	int numClients;
	int middleWareWorkers;
	int dbWorkers = 3;
	double clientThinkTime = 0.0025;
	double middleWareServiceTime = 0.000;
	double dbServiceTime = 0.0105;
	int dbQueueCapacity = -1;

	public String toString() {
		String result = "CLIENTS-" + numClients + " WORKERS-"
				+ middleWareWorkers + " dbWorkers: " + dbWorkers
				+ " middleWareServiceTime: " + middleWareServiceTime
				+ " dbServiceTime: " + dbServiceTime + ", dbQueueCapacity: "
				+ dbQueueCapacity;
		return result;
	}
}

class DBLConfiguration extends Configuration{
	public DBLConfiguration(int middleWareWorkers, int numClients) {
		super(middleWareWorkers, numClients);
		dbWorkers = 4;
		dbServiceTime = 0.0015;
	}
}