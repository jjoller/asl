package asl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Used to merge machine logs from clients and message servers into one file.
 * 
 * @author jost
 *
 */
public class MergeLogs {

	public static void main(String[] args) throws IOException {

		File folder, log;
		
		folder = new File("logs/dbThroughput:1sD_2sS_4sC");
		log = new File("dbThroughput.csv");
		writeLine(log,header());
		mergeLogs(folder,log, 1,1);
		
		folder = new File("logs/serverThroughput:12xlD_1sS_2lC");
		log = new File("serverThroughput.csv");
		writeLine(log,header());
		mergeLogs(folder,log, 10,1);
		
		folder = new File("logs/serverThroughputRef:12xlD_1sS_2lC");
		log = new File("serverThroughputRef.csv");
		writeLine(log,header());
		mergeLogs(folder,log, 10,1);
		
		folder = new File("logs/serverThroughput:12xlD_1sS_2lC");
		log = new File("serverThroughput.csv");
		writeLine(log,header());
		mergeLogs(folder,log, 10,1);
		
		folder = new File("logs/stability:1sD_1sS_4sC");
		log = new File("stability.csv");
		writeLine(log,header());
		mergeLogs(folder,log, 10,1);
		
		folder = new File("logs/messageSize:1sD_1sS_4sC");
		log = new File("msgSize.csv");
		writeLine(log,header());
		mergeLogs(folder,log, 2,1);
		
		folder = new File("logs/dbScaleability:1sD_1sS_4s_C");
		log = new File("dbScale.csv");
		writeLine(log,header());
		mergeLogs(folder,log, 2,1);
		
	}
	
	public static void writeLine(File file, String line) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file));) {
			writer.write(line + "\n");
		}
	}

	public static String header() {
		String header = "servers,threads,clients,msgLength,timeStamp,requestsPerClient,failedRequests,clientThinkTime,waitingForConnectionTime,upstreamTime,waitingForResponseTime,";
		header += "timeStamp,totalRequests,failedRequests,totalHandleTime,upstreamTime,downstreamTime,CREATE_QUEUE_time,DELETE_QUEUE_time,SEND_MESSAGE_time,RECEIVE_MESSAGE_time,POP_QUEUE_time,TOP_QUEUE_time,INBOX_QUEUES_time";
		return header;
	}

	public static void mergeLogs(File folder, File log, int omittedStartLines,int omittedEndLines) throws IOException {
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				mergeLogs(fileEntry,log,omittedStartLines,omittedEndLines);
			} else {
				
				if(isValidLogFileName(fileEntry.getName())){
					int s = servers(fileEntry.getName());
					int t = threads(fileEntry.getName());
					int c = clients(fileEntry.getName());
					int m = messageLength(fileEntry.getName());
					writeCSV(s,t,c,m,log,fileEntry,omittedStartLines,omittedEndLines);
					System.out.println("merged log: "+fileEntry.getName());
				}
			}
		}
	}
	
	protected static boolean isValidLogFileName(String name){
		
		boolean result = !name.contains("~");
		result = result && !name.contains(".");
		result = result && name.contains("S");
		result = result && name.contains("T");
		result = result && name.contains("C");
		result = result && name.contains("M");
		return result;
	}
	
	protected static int servers(String fileName){
		return new Integer(fileName.split("S")[0]);
	}

	protected static int threads(String fileName){
		return new Integer(fileName.split("T")[0].split("S")[1]);
	}
	
	protected static int clients(String fileName){
		return new Integer(fileName.split("C")[0].split("T")[1]);
	}

	protected static int messageLength(String fileName){
		return new Integer(fileName.split("M")[0].split("C")[1]);
	}
	
	protected static void writeCSV(int servers, int threads, int clients,
			int msgLength, File log, File source, int omittedStartLines, int omittedEndLines) throws IOException {

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(log, true));
				FileReader reader = new FileReader(source);
				BufferedReader br = new BufferedReader(new FileReader(source));) {

			List<String> clientLogs = new ArrayList<String>();
			List<String> serverLogs = new ArrayList<String>();

			int omitStart = omittedStartLines;
			
			boolean readClient = false;
			boolean readServer = false;
			
			String line = br.readLine();
			while (line != null) {
								
				if (line.matches(".*[a-zA-Z]+.*") ||  line.length() <= 10) {
					omitStart = omittedStartLines;
					if(readClient){
						clientLogs = clientLogs.subList(0, clientLogs.size()-omittedEndLines);
						readClient = false;
					}
					if(readServer){
						serverLogs = serverLogs.subList(0, serverLogs.size()-omittedEndLines);
						readServer = false;
					}
				}else {
					if(omitStart <= 0){
						
						if(line.contains("Error")){
							throw new IllegalStateException(line);
						}
						
						String[] splits = line.split(",");
						if (splits.length > 7) {
							readServer = true;
							// server log
							// multiply total request by the number of server threads
							double totalRequests = new Double(splits[1]);
							totalRequests *= threads;
							line = line.replaceFirst(splits[1], totalRequests+"");
							serverLogs.add(line);
						} else {
							readClient = true;
							// client log
							clientLogs.add(line);
						}
					}
					omitStart --;
				}
				line = br.readLine();
			}

			// do not use the first and the last line of each log file
			for (int i = 0; i < Math.min(clientLogs.size(), serverLogs.size()); i++) {
				writer.write(servers+","+threads+","+clients+","+msgLength+","+clientLogs.get(i) + "," + serverLogs.get(i)+"\n");
			}

		}
	}
}

