package asl;

public class DBException extends Exception {

	private static final long serialVersionUID = 584784270562264763L;

	public DBException(String msg) {
		super(msg);
	}

}
