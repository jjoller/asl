package asl;

import java.util.Iterator;
import java.util.Set;

/**
 * Utility functions.
 * 
 * @author jost
 *
 */
public class Utils {

	// An arbitrary string which can be used to form test messages
	private static final String loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer faucibus facilisis leo posuere luctus. In tristique erat turpis, eget commodo nisl ullamcorper malesuada. Donec nisl lorem, convallis ac eros sed, suscipit eleifend neque. Ut ut nisi nisl. Etiam id justo sit amet lorem eleifend aliquet. Suspendisse potenti. Nunc at ullamcorper mauris. Morbi vitae congue nisi.";

	/**
	 * Return a random lorem ipsum string of a certain length.
	 * 
	 * @param length
	 * @return
	 */
	public static String loremIpsum(int length) {

		String s = "";
		while (s.length() < length) {

			int a = RandomSeed.random.nextInt(loremIpsum.length() - 10);
			int b = RandomSeed.random.nextInt(loremIpsum.length() - 1);

			int begin = Math.min(a, b);
			int max = Math.max(a, b) + 1;

			s += loremIpsum.substring(begin, max);
		}

		s = s.substring(0, length);

		// postcondition
		if (s.length() != length) {
			throw new IllegalStateException();
		}

		return s;
	}

	public static int getRandom(Set<Integer> clients) {

		int a = RandomSeed.random.nextInt(clients.size());
		Iterator<Integer> iter = clients.iterator();
		int b = iter.next();
		for (int i = 0; i < a; i++) {
			b = iter.next();
		}
		return b;
	}

	public static int[] toArray(Set<Integer> clients) {
		int[] a = new int[clients.size()];
		Iterator<Integer> iter = clients.iterator();
		int i = 0;
		while (iter.hasNext()) {
			a[i++] = iter.next();
		}
		return a;
	}

}
