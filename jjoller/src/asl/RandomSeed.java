package asl;

import java.util.Random;

/**
 * 
 * Allows to set the random seed for random number generators. Random number
 * generators should access their seed from this class to make the output of the
 * application reproducible.
 * 
 */
public class RandomSeed {

	private static Random seedProvider;
	public static final Random random;

	static {
		seedProvider = new Random();
		random = new Random(getSeed());
	}

	public static void setSeed(long seed) {
		seedProvider.setSeed(seed);
		random.setSeed(getSeed());
	}

	public static long getSeed() {
		return RandomSeed.seedProvider.nextLong();
	}
}
