package asl;

public class HelloASL
{
    public static void main(String[] args) {
        //Simple output
        System.out.println("HelloASL");

        //Read arguments
//        assert(args.length >= 2);
//        System.out.println("Arg0: " + args[0]);
//        System.out.println("Arg1: " + args[1]);

        //Check if JDBC library is in classpath
        try {
			Class.forName("org.postgresql.Driver");
			System.out.println("JDBC Driver found");
		} catch(ClassNotFoundException e) {
			System.out.println("JDBC Driver not found!!!");
		}
    }
}