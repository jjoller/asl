package asl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;

public class DBConnectionPool {

	public DBConnectionPool(String dbServer, int connections)
			throws SQLException {

		this.dbServer = dbServer;
		connectionPool = new ArrayBlockingQueue<Connection>(connections, true);
		for (int i = 0; i < connections; i++) {
			connectionPool.add(connection());
		}
	}

	private final String dbServer;
	private final ArrayBlockingQueue<Connection> connectionPool;

	protected Connection connection() throws SQLException {
		return java.sql.DriverManager.getConnection(dbURL(), DAO.DATABASE_USER,
				DAO.DATABASE_PASSWORD);
	}

	public String dbURL() {
		return "jdbc:postgresql://" + dbServer + "/" + DAO.DATABASE_NAME;
	}

	public Connection acquireConnection() {
		try {
			return connectionPool.take();
		} catch (InterruptedException e) {
			throw new IllegalStateException(e);
		}
	}

	public void releaseConnection(Connection connection) {
		if (connection != null) {
			try {
				connectionPool.put(connection);
			} catch (InterruptedException e) {
				throw new IllegalStateException(e);
			}
		}
	}

	public void closeConnections() throws SQLException {
		for (Connection c : connectionPool) {
			c.close();
		}
	}

}
