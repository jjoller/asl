package asl.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import asl.DAO;
import asl.logging.LogDAO;
import asl.message.ErrorMessages;
import asl.message.Request;
import asl.message.Response;
import asl.server.handlers.RequestHandler;

/**
 * The server instance which provides the interface for the clients. Listens on
 * a certain port and processes incoming client requests.
 * 
 * @author jost
 *
 */
public class MessageServer implements Runnable {

	private static final Logger log = Logger.getLogger(MessageServer.class
			.getName());
	public static final int PORT = 9000;

	public MessageServer(DAO dao, LogDAO logDAO, int threads,
			int runningTimeSeconds) throws SQLException {

		executorService = Executors.newFixedThreadPool(threads);
		this.runningTimeMillis = runningTimeSeconds * 1000;
		this.dao = dao;
		this.logDAO = logDAO;
	}

	private ServerSocket serverSocket;
	private int runningTimeMillis;
	private DAO dao;
	private LogDAO logDAO;

	private final ExecutorService executorService;

	/**
	 * run the message server
	 */
	public void run() {

		long startTime = 0;
		ServerLog theLog = new ServerLog();

		try {

			serverSocket = new ServerSocket(PORT);

			log.info("server host name: "
					+ serverSocket.getInetAddress().getHostName()
					+ "\nHost address: "
					+ serverSocket.getInetAddress().getHostAddress());

			log.info(serverSocket.toString());
			while (startTime == 0
					|| (System.currentTimeMillis() - startTime < runningTimeMillis)) {
				Socket clientSocket = this.serverSocket.accept();
				executorService.submit(new Job(dao, clientSocket, theLog));

				// the clock starts ticking when the first request is received.
				if (startTime == 0) {
					startTime = System.currentTimeMillis();
				}
			}

			log.info("Server does not accept any more requests, write logs...");

			logDAO.saveLog(theLog);
			log.info(theLog.toString());

			log.info("Stored the server logs on the db");

			log.info("start termination phase");

			final ServerSocket ss = serverSocket;
			// termination phase
			Thread stopThread = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						while (true) {
							Socket clientSocket;
							clientSocket = ss.accept();

							executorService.submit(new SendTerminationMessage(
									clientSocket));
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});

			stopThread.start();
			Thread.sleep(2000);
			log.info("interrupt termination thread thread");
			stopThread.interrupt();

		} catch (IOException | SQLException | InterruptedException e) {
			log.log(Level.SEVERE, e.getMessage(), e);
			theLog.logError(e);
		} finally {
			executorService.shutdownNow();
			try {
				serverSocket.close();
			} catch (IOException e) {
				log.log(Level.SEVERE, e.getMessage(), e);
				theLog.logError(e);
			}
			// db is not needed any more, close connections
			try {
				dao.getConnectionPool().closeConnections();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

class Job implements Runnable {

	private static final Logger log = Logger
			.getLogger(Runnable.class.getName());

	public Job(DAO dao, Socket clientSocket, ServerLog theLog)
			throws SQLException {
		this.clientSocket = clientSocket;
		handler = new RequestHandler(dao);
		this.theLog = theLog;
	}

	protected RequestHandler handler;
	protected Socket clientSocket;
	protected ServerLog theLog;

	public void run() {

		long start = System.currentTimeMillis();
		long readRequestTime = 0;
		long handleRequestTime = 0;
		Request req = null;

		ObjectOutputStream oos = null;
		InputStream input = null;
		ObjectInputStream ois = null;
		OutputStream output = null;
		Response resp = null;
		try {
			input = clientSocket.getInputStream();
			output = clientSocket.getOutputStream();

			ois = new ObjectInputStream(input);
			req = (Request) ois.readObject();

			// System.out.println(req);

			readRequestTime = System.currentTimeMillis() - start;
			start = System.currentTimeMillis();

			req.setArrivalTime(new Timestamp(System.currentTimeMillis()));

			resp = handler.handleRequests(req);

			handleRequestTime = System.currentTimeMillis() - start;
			start = System.currentTimeMillis();

			oos = new ObjectOutputStream(output);
			oos.writeObject(resp);
			oos.flush();
		} catch (IOException | ClassNotFoundException e) {
			log.log(Level.SEVERE, e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("exception " + e.getMessage());
		} finally {
			// close everything which is closeable
			try {
				if (output != null) {
					output.close();
				}
				if (input != null) {
					input.close();
				}
				if (ois != null) {
					ois.close();
				}
				clientSocket.close();
			} catch (IOException e) {
				log.log(Level.SEVERE, e.getMessage());
				e.printStackTrace();
			}
		}
		long sendResponseTime = System.currentTimeMillis() - start;
		if (req != null) {
			theLog.logRequest(req.getOperation(), readRequestTime,
					handleRequestTime, sendResponseTime);
		}
	}
}

class SendTerminationMessage implements Runnable {

	public SendTerminationMessage(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	protected Socket clientSocket;

	public void run() {
		try (OutputStream output = clientSocket.getOutputStream();
				InputStream input = clientSocket.getInputStream();
				ObjectInputStream ois = new ObjectInputStream(input);
				ObjectOutputStream oos = new ObjectOutputStream(output);) {

			Request req = (Request) ois.readObject();
			Response resp = new Response(req);
			resp.setError(ErrorMessages.SERVER_TERMINATED.getMsg());
			oos.writeObject(resp);
			oos.flush();

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
