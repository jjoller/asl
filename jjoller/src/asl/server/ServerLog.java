package asl.server;

import java.util.HashMap;
import java.util.Map;

import asl.logging.Log;
import asl.logging.RunningStat;
import asl.message.Operation;

public class ServerLog extends Log {

	public ServerLog() {
		super();
		reset();
		writeCSVHeader();
	}

	synchronized public void logError(Exception e) {
		super.logError(e);
		totalRequests++;
		failedRequests++;
	}

	private Map<Operation, RunningStat> operationStats;
	private long totalRequests = 0;
	private long failedRequests = 0;
	private RunningStat upStreamTime = new RunningStat(); // time spent for
															// transferring
															// requests from
	// client to server.
	private RunningStat downStreamTime = new RunningStat(); // time spent for
															// transferring
															// responses
	// from server to client.
	private long totalHandleTime = 0;

	private void reset() {

		operationStats = new HashMap<Operation, RunningStat>();

		for (Operation o : Operation.values()) {
			operationStats.put(o, new RunningStat());
		}

		totalRequests = 0;
		failedRequests = 0;
		upStreamTime = new RunningStat();
		totalHandleTime = 0;
		downStreamTime = new RunningStat();
		intervalStart = System.currentTimeMillis();
	}

	/**
	 * Log a request.
	 * 
	 * @param operation
	 *            The operation which is requested by the client.
	 * @param upstreamTime
	 *            The time spent for transferring the request from client to
	 *            server.
	 * @param handleRequestTime
	 *            The time spent for handling the request on the server.
	 * @param downstreamTime
	 *            The time spent for transferring the response from server to
	 *            client.
	 */
	synchronized public void logRequest(Operation operation, long upstreamTime,
			long handleTime, long downstreamTime) {

		totalRequests++;
		this.upStreamTime.push(upstreamTime);
		this.downStreamTime.push(downstreamTime);
		this.totalHandleTime += handleTime;

		operationStats.get(operation).push(handleTime);

		if (System.currentTimeMillis() - intervalStart > loggingInterval) {
			writeCSVLog();
			reset();
		}
	}

	private void writeCSVLog() {

		double seconds = ((double) this.loggingInterval) / 1000.0;

		log += System.currentTimeMillis();
		log += "," + ((double) totalRequests) / seconds;
		log += "," + ((double) failedRequests) / seconds;
		log += "," + ((double) totalHandleTime) / seconds;
		log += "," + upStreamTime.mean() + "," + upStreamTime.variance();
		log += "," + downStreamTime.mean() + "," + upStreamTime.variance();
		for (Operation o : Operation.values()) {
			log += "," + operationStats.get(o).numDataValues() + ","
					+ operationStats.get(o).mean() + ","
					+ operationStats.get(o).variance();
		}
		log += "\n";
	}

	private void writeCSVHeader() {

		this.log = this.log
				+ "timeStamp,totalRequests,failedRequests,totalHandleTime,upstreamTime,upstreamTimeVar,downstreamTime,downstreamTimeVar";
		for (Operation o : Operation.values()) {
			this.log += "," + o.name() + "_N";
			this.log += "," + o.name() + "_time";
			this.log += "," + o.name() + "_timeVar";
		}
		this.log += "\n";
	}

}
