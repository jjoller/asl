package asl.server.handlers;

import java.sql.SQLException;
import java.util.Set;

import asl.DAO;
import asl.Utils;
import asl.message.ErrorMessages;
import asl.message.Message;
import asl.message.Request;
import asl.message.Response;

/**
 * Handles requests by delegating calls to the database object.
 * 
 * @author jost
 *
 */
public class RequestHandler {

	public RequestHandler(DAO dao) throws SQLException {
		this.dao = dao;
	}

	public static final int DUMMY_CLIENT = 7777777;

	private DAO dao;

	public Response handleRequests(Request req) {

		Response resp = null;

		switch (req.getOperation()) {
		case CREATE_CLIENT:
			resp = createClient(req);
			break;
		case CREATE_QUEUE:
			resp = createQueue(req);
			break;
		case DELETE_QUEUE:
			resp = deleteQueue(req);
			break;
		case SEND_MESSAGE:
			resp = sendMessage(req);
			break;
		case RECEIVE_MESSAGE:
			resp = receiveMessage(req);
			break;
		case POP_QUEUE:
			resp = popQueue(req);
			break;
		case TOP_QUEUE:
			resp = topQueue(req);
			break;
		case INBOX_QUEUES:
			resp = inboxQueues(req);
			break;
		case CLIENT_LIST:
			resp = clientList(req);
			break;
		case MESSAGE_COUNT:
			resp = messageCount(req);
			break;
		case QUEUE_LIST:
			resp = this.queueList(req);
			break;

		default:
			resp = new Response();
			resp.setError(ErrorMessages.UNSPECIFIED_OPERATION.getMsg());
		}
		return resp;
	}

	/**
	 * Get the total number of clients in a queue or of all queues
	 * 
	 * @param req
	 * @return
	 */
	private Response messageCount(Request req) {

		Response resp = new Response(req);
		int count;
		if (req.getQueue() > 0) {
			count = dao.numMessages(req.getQueue());
		} else {
			count = dao.numMessages();
		}
		resp.setMessage(count + "");
		return resp;
	}

	/**
	 * Receive the list of clients in the system.
	 * 
	 * @param req
	 * @return
	 */
	private Response clientList(Request req) {

		Response resp = new Response(req);
		Set<Integer> clients = dao.clients();
		resp.setQueues(Utils.toArray(clients));
		return resp;
	}

	/**
	 * This is usually the first call from a client
	 * 
	 * @param req
	 * @return
	 */
	private Response createClient(Request req) {

		Response resp = new Response(req);
		int clientId = dao.createClient();
		resp.setSender(clientId);
		resp.setReceiver(clientId);
		return resp;
	}

	private Response createQueue(Request req) {

		int queue = dao.createQueue();
		Response resp = new Response(req);
		resp.setQueue(queue);
		return resp;
	}

	private Response deleteQueue(Request req) {

		Response resp = new Response(req);
		int queue = req.getQueue();
		if (queue <= 0) {
			resp.setError(ErrorMessages.ILLEGAL_QUEUE_IDENTIFIER.getMsg());
		} else {
			try {
				dao.deleteQueue(req.getQueue());
			} catch (Exception e) {
				resp.setError(e.getMessage());
			}
		}
		return resp;
	}

	private Response sendMessage(Request req) {

		Response resp = new Response(req);
		if (req.getQueue() <= 0) {
			resp.setError(ErrorMessages.ILLEGAL_QUEUE_IDENTIFIER.getMsg());
		} else {
			try {
				dao.saveMessage(req);
			} catch (Exception e) {
				resp.setError(e.getMessage());
			}
		}
		return resp;
	}

	private Response receiveMessage(Request req) {

		Message msg = dao.messageFromSender(req.getSender(), req.getReceiver());
		Response resp;
		if (msg != null) {
			resp = new Response(msg);
		} else {
			resp = new Response(req);
			resp.setError(ErrorMessages.NO_SUCH_MESSAGE.getMsg());
		}
		return resp;
	}

	private Response popQueue(Request req) {

		Response resp;
		if (req.getQueue() <= 0) {
			resp = new Response(req);
			resp.setError(ErrorMessages.ILLEGAL_QUEUE_IDENTIFIER.getMsg());
		} else {
			Message msg = dao.popQueue(req.getQueue(), req.getReceiver());
			if (msg != null) {
				resp = new Response(msg);
			} else {
				resp = new Response(req);
				resp.setError(ErrorMessages.NO_SUCH_MESSAGE.getMsg());
			}
		}
		return resp;
	}

	private Response topQueue(Request req) {

		Response resp;
		if (req.getQueue() <= 0) {
			resp = new Response(req);
			resp.setError(ErrorMessages.ILLEGAL_QUEUE_IDENTIFIER.getMsg());
		} else {
			Message msg = dao.topQueue(req.getQueue(), req.getReceiver());
			if (msg != null) {
				resp = new Response(msg);
			} else {
				resp = new Response(req);
				resp.setError(ErrorMessages.NO_SUCH_MESSAGE.getMsg());
			}
		}
		return resp;
	}

	private Response inboxQueues(Request req) {

		Set<Integer> inboxQueues = dao.inboxQueues(req.getReceiver());
		Response resp = new Response(req);
		resp.setQueues(Utils.toArray(inboxQueues));
		return resp;
	}

	private Response queueList(Request req) {

		Response resp = new Response(req);
		resp.setQueues(Utils.toArray(dao.queues()));
		return resp;
	}

}
