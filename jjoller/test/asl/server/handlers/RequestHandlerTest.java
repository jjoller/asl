package asl.server.handlers;

import asl.DAO;
import asl.DBConnectionPool;
import asl.Test;
import asl.Testsuite;
import asl.Utils;
import asl.message.ErrorMessages;
import asl.message.Operation;
import asl.message.Request;
import asl.message.Response;

public class RequestHandlerTest extends Test {

	public RequestHandlerTest(Testsuite suite) {
		super(suite);
	}

	@Override
	protected void executeTest() throws Exception {

		DBConnectionPool pool = new DBConnectionPool(TEST_SERVER_URL, 4);
		DAO dao = new DAO(pool);

		dao.resetDatabase();

		RequestHandler handler = new RequestHandler(dao);

		Request req = new Request();
		req.setOperation(Operation.DELETE_QUEUE);
		req.setQueue(-1);

		Response resp = handler.handleRequests(req);

		assertEquals("illegal queue id", resp.getError(),
				ErrorMessages.ILLEGAL_QUEUE_IDENTIFIER.getMsg());
		assertFalse("queue does not exist", dao.queueExists(req.getQueue()));

		req.setOperation(Operation.CREATE_QUEUE);
		req.setQueue(1);
		resp = handler.handleRequests(req);

		assertTrue("no error when creating queue", resp.getError() == null);
		assertTrue("queue exists", dao.queueExists(req.getQueue()));

		req.setOperation(Operation.SEND_MESSAGE);
		req.setQueue(7);
		req.setMessage("blabla");
		req.setReceiver(1);
		req.setSender(2);

		assertFalse("queue 7 does not exist", dao.queueExists(7));
		resp = handler.handleRequests(req);
		assertFalse("queue 7 does still not exist", dao.queueExists(7));
		assertEquals("error when sending message to non existing queue",
				ErrorMessages.QUEUE_NOT_EXIST.getMsg(), resp.getError());

		req.setQueue(1);
		resp = handler.handleRequests(req);
		assertTrue("no error when sending message to existing queue",
				resp.getError() == null);

		req = new Request();
		req.setOperation(Operation.RECEIVE_MESSAGE);
		req.setReceiver(3);
		req.setSender(4);
		resp = handler.handleRequests(req);
		assertEquals("error when asking for non-existing message",
				resp.getError(), ErrorMessages.NO_SUCH_MESSAGE.getMsg());

		Request sendMsg = new Request();
		sendMsg.setMessage(Utils.loremIpsum(100));
		sendMsg.setOperation(Operation.SEND_MESSAGE);
		sendMsg.setReceiver(3);
		sendMsg.setSender(4);
		sendMsg.setQueue(1);
		resp = handler.handleRequests(sendMsg);
		assertTrue("no error when sending another message to existing queue",
				resp.getError() == null);

		resp = handler.handleRequests(req);
		assertEquals("message is the same", resp.getMessage(),
				sendMsg.getMessage());
		assertTrue("no error when receiving existing message",
				resp.getError() == null);

	}
}
