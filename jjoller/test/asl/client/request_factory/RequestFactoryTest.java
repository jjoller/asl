package asl.client.request_factory;

import java.util.logging.Logger;

import asl.RandomSeed;
import asl.Test;
import asl.Testsuite;
import asl.message.MessageDAOTest;
import asl.message.Response;

public class RequestFactoryTest extends Test {

	public RequestFactoryTest(Testsuite suite) {
		super(suite);
	}

	private static final Logger log = Logger.getLogger(MessageDAOTest.class
			.getName());

	@Override
	public void executeTest() {

		RandomSeed.setSeed(2134);
		int n = 100;
		RandomRequestFactory messageFactory = new RandomRequestFactory(1, 10,
				100, 5);
		for (int i = 0; i < n; i++) {
			Response response = new Response();
			log.info(messageFactory.nextRequest(response).toString() + "\n");
		}
	}

}
