package asl;

public abstract class Test {

	protected static final String TEST_SERVER_URL = "localhost";

	public Test(Testsuite suite) {
		this.suite = suite;
	}

	private Testsuite suite;

	protected void assertTrue(String descr, boolean expression) {
		if (expression) {
			suite.publishSuccess(descr);
		} else {
			suite.publishFail(descr);
		}
	}

	protected void assertFalse(String descr, boolean expression) {
		if (!expression) {
			suite.publishSuccess(descr);
		} else {
			suite.publishFail(descr);
		}
	}

	protected void assertEquals(String descr, Object o1, Object o2) {
		if (o1.equals(o2)) {
			suite.publishSuccess(descr);
		} else {
			suite.publishFail(descr + ", '" + o1.toString() + "' NOT EQUALS '"
					+ o2.toString() + "'");
		}
	}

	public void test() {
		try {
			executeTest();
		} catch (Exception e) {
			suite.publishFail(e.getMessage());
			e.printStackTrace();
		}
	}

	protected abstract void executeTest() throws Exception;

}
