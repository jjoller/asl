package asl;

import java.util.LinkedList;
import java.util.List;

import asl.client.request_factory.RequestFactoryTest;
import asl.logging.LogDAOTest;
import asl.logging.LogTest;
import asl.message.InboxQueuesTest;
import asl.message.MessageDAOTest;
import asl.message.MessagesOverviewTest;
import asl.message.StoredProcedureTest;
import asl.server.handlers.RequestHandlerTest;

/**
 * Executes the tests.
 * 
 * @author jost
 *
 */
public class Testsuite {

	public static void main(String[] args) {
		new Testsuite();
	}

	/**
	 * Tests have to be in here.
	 */
	public Testsuite() {
		
		new InboxQueuesTest(this).test();
//		new StoredProcedureTest(this).test();
//		new LogDAOTest(this).test();
//		new RequestFactoryTest(this).test();
//		new MessageDAOTest(this).test();
//		new LogTest(this).test();
//		new RequestHandlerTest(this).test();
//		new MessagesOverviewTest(this).test();
		printResults();
	}

	private void printResults() {
		for (String s : successes) {
			System.out.println(s);
		}
		for (String s : fails) {
			System.out.println(s);
		}
	}

	private List<String> successes = new LinkedList<String>();
	private List<String> fails = new LinkedList<String>();

	protected void publishSuccess(String testDescr) {
		successes.add("pass: " + testDescr);
	}

	protected void publishFail(String testDescr) {
		fails.add("FAIL: " + testDescr);
	}

}
