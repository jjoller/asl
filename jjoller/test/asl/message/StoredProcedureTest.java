package asl.message;

import java.sql.Timestamp;
import java.util.logging.Logger;

import asl.DAO;
import asl.DBConnectionPool;
import asl.Test;
import asl.Testsuite;

public class StoredProcedureTest extends Test {

	private static final Logger log = Logger.getLogger(MessageDAOTest.class
			.getName());

	public StoredProcedureTest(Testsuite suite) {
		super(suite);
	}

	@Override
	protected void executeTest() throws Exception {

		log.info("Test the " + DAO.class + " class");

		DBConnectionPool pool = new DBConnectionPool(TEST_SERVER_URL,
				4);
		DAO dao = new DAO(pool);

		// start from a clean database
		dao.resetDatabase();
		
		

		Message message = new Message();
		int q = dao.createQueue();
		message.setQueue(q);
		message.setSender(1);
		message.setReceiver(2);
		message.setArrivalTime(new Timestamp(System.currentTimeMillis()));
		message.setMessage("Does the stored procedure work?");
		dao.saveMessage(message);
		Message msg = dao.topQueue(q, 2);
		System.out.println(msg);

		Message m = dao.popQueue(q, 2);
		System.out.println(m);
	}

}
