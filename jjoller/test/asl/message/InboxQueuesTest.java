package asl.message;

import java.util.Set;

import asl.DAO;
import asl.DBConnectionPool;
import asl.Test;
import asl.Testsuite;
import asl.server.handlers.RequestHandler;

public class InboxQueuesTest extends Test {

	public InboxQueuesTest(Testsuite suite) {
		super(suite);
	}

	@Override
	protected void executeTest() throws Exception {

		DBConnectionPool pool = new DBConnectionPool(TEST_SERVER_URL, 4);
		DAO dao = new DAO(pool);

		// start from a clean database
		dao.resetDatabase();

		int q1 = dao.createQueue();
		dao.createQueue();
		int q3 = dao.createQueue();
		dao.createQueue();

		Message message = new Message();
		message.setReceiver(3);
		message.setQueue(q1);
		message.setSender(1);
		dao.saveMessage(message);

		message = new Message();
		message.setReceiver(3);
		message.setQueue(q3);
		message.setSender(1);
		dao.saveMessage(message);

		Set<Integer> inboxes = dao.inboxQueues(3);
		assertTrue("inbox queue 1 is found", inboxes.contains(q1));
		assertTrue("inbox queue 3 is found", inboxes.contains(q3));

		Request req = new Request();
		req.setOperation(Operation.INBOX_QUEUES);
		req.setReceiver(3);
		req.setSender(1);

		Response resp = new RequestHandler(dao).handleRequests(req);
		assertTrue("message handler returns 2 queues",
				resp.getQueues().length == 2);
		assertTrue("found1", resp.getQueues()[0] == q1
				|| resp.getQueues()[0] == q3);
		assertTrue("found2", (resp.getQueues()[1] == q1)
				|| (resp.getQueues()[1] == q3));
	}
}
