package asl.message;

import asl.DAO;
import asl.DBConnectionPool;
import asl.Test;
import asl.Testsuite;

public class MessagesOverviewTest extends Test {

	public MessagesOverviewTest(Testsuite suite) {
		super(suite);
	}

	@Override
	protected void executeTest() throws Exception {

		DBConnectionPool pool = new DBConnectionPool(TEST_SERVER_URL,
				4);
		MessagesOverview mo = new MessagesOverview(new DAO(pool));
		assertFalse("is not null ", mo == null);
		System.out.println(mo);
	}

}
