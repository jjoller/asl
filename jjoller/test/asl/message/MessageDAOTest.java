package asl.message;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import asl.DAO;
import asl.DBConnectionPool;
import asl.DBException;
import asl.Test;
import asl.Testsuite;
import asl.Utils;
import asl.message.Message;
import asl.message.MessageFactory;

public class MessageDAOTest extends Test {

	public MessageDAOTest(Testsuite suite) {
		super(suite);
	}

	private static final Logger log = Logger.getLogger(MessageDAOTest.class
			.getName());

	@Override
	public void executeTest() throws SQLException, DBException {

		log.info("Test the " + DAO.class + " class");

		int n = 10;

		DBConnectionPool pool = new DBConnectionPool(TEST_SERVER_URL,
				4);
		DAO dao = new DAO(pool);

		// start from a clean database
		dao.resetDatabase();

		List<Integer> queues = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			queues.add(dao.createQueue());
		}

		// put some messages into the database
		// amount, length, sender, receiver, queue
		List<Message> messages = new MessageFactory().testMessages(n, 2000, 1,
				2, queues.get(0));

		long minTime = Long.MAX_VALUE;

		for (Message message : messages) {
			dao.saveMessage(message);
			minTime = Math.min(message.getArrivalTime().getTime(), minTime);
		}

		// test get all messages
		List<Message> allMessages = dao.allMessages();
		assertTrue("retrieved all messages", allMessages.size() == n);

		// test top queue (get the oldest from queue)
		Message message = dao.topQueue(1, 2);
		assertTrue("message is not null", message != null);
		assertTrue("is oldest message",
				message.getArrivalTime().getTime() == minTime);

		// test delete message
		dao.deleteMessage(message);
		assertTrue("message deleted", dao.allMessages().size() == n - 1);

		dao.createClient();
		dao.createClient();
		
		// test message from sender
		assertTrue("sender exists", dao.clientExists(1));
		assertTrue("receiver exists", dao.clientExists(2));
		assertFalse("3 does not exist", dao.clientExists(3));

		Message msg = dao.messageFromSender(1, 2);
		assertTrue("message not null", msg != null);
		assertTrue("message is from sender", msg.getSender() == 1);
		assertTrue("message is for receiver", msg.getReceiver() == 2);
		msg = dao.messageFromSender(2, 1);
		assertTrue("message is null", msg == null);

		// test receiving broadcast
		Message broadcast = new Message();
		broadcast.setSender(3);
		broadcast.setMessage(Utils.loremIpsum(100));
		broadcast.setQueue(1);
		dao.saveMessage(broadcast);
		msg = dao.messageFromSender(3, 17);
		assertTrue("broadcast received", msg != null);

		// // test delete queue
		// assertTrue("queue exists", dao.queueExists(1));
		// dao.deleteQueue(1);
		// assertTrue("no messages", dao.allMessages().size() == 0);
		// msg = dao.messageFromSender(1, 2);
		// assertTrue("no messages", msg == null);
		// assertFalse("queue does not exist", dao.queueExists(1));
		//
		// // possible to "delete" non-existing queue
		// assertFalse("queue does not exist", dao.queueExists(3));
		// dao.deleteQueue(3);

		// test inbox queues
		Message m1 = new Message();
		m1.setQueue(2);
		m1.setSender(1);
		m1.setReceiver(3);

		Message m2 = new Message();
		m2.setQueue(3);
		m2.setSender(10);
		m2.setReceiver(3);

		Message m3 = new Message();
		m3.setQueue(4);
		m3.setSender(10);
		m3.setReceiver(4);

		// a broadcast
		Message m4 = new Message();
		m4.setQueue(5);
		m4.setSender(10);
		m4.setReceiver(0);

		dao.saveMessage(m1);
		dao.saveMessage(m2);
		dao.saveMessage(m3);
		dao.saveMessage(m4);

		Set<Integer> inboxQueues = dao.inboxQueues(3);
		assertTrue("2 is inbox queue", inboxQueues.contains(2));
		assertTrue("3 is inbox queue", inboxQueues.contains(3));
		assertFalse("4 is not inbox queue", inboxQueues.contains(4));
		assertTrue("5 is inbox queue", inboxQueues.contains(5));

		assertEquals("num messages", dao.numMessages(), 14);
		assertEquals("senders", dao.senders().size(), 3);
		assertEquals("receivers", dao.receivers().size(), 4);

		Set<Integer> qs = dao.queues();
		assertEquals("queues", qs.size(), 10);

		assertTrue("queues contains 2", qs.contains(2));
		assertTrue("queues contains 3", qs.contains(3));
		assertTrue("queues contains 4", qs.contains(4));
		assertTrue("queues contains 5", qs.contains(5));

		assertEquals("num messages in queue 5 is 1", dao.numMessages(5), 1);

	}
}