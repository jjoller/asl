package asl.logging;

import java.util.Random;
import java.util.logging.Logger;

import asl.RandomSeed;
import asl.Test;
import asl.Testsuite;
import asl.message.Operation;
import asl.server.ServerLog;

public class LogTest extends Test {

	private static final Logger log = Logger.getLogger(LogTest.class.getName());

	public LogTest(Testsuite suite) {
		super(suite);
	}

	@Override
	protected void executeTest() throws Exception {
		
		RandomSeed.setSeed(348);

		Random r = new Random(RandomSeed.getSeed());

		ServerLog theLog = new ServerLog();

		int maxTime = 1000;
		theLog.setLoggingInterval(10);
		while(theLog.getLog().length() < 1000){
			theLog.logRequest(
					Operation.values()[r.nextInt(Operation.values().length)],
					r.nextInt(maxTime), r.nextInt(maxTime), r.nextInt(maxTime));
		}

		log.info(theLog + "");

		assertFalse("log does not contain null",
				theLog.getLog().contains("null"));

		assertFalse("log does not contain 0",theLog.getLog().contains(",0,"));
		
	}

}
