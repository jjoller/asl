package asl.logging;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.logging.Logger;

import asl.DBConnectionPool;
import asl.Main;
import asl.Test;
import asl.Testsuite;
import asl.Utils;

/**
 * Test the LogDAO
 * 
 * @author jost
 *
 */
public class LogDAOTest extends Test {

	public LogDAOTest(Testsuite suite) {
		super(suite);
	}

	private static final Logger log = Logger.getLogger(LogDAOTest.class
			.getName());

	@Override
	public void executeTest() throws SQLException {

		LogDAO dao = new LogDAO(new DBConnectionPool(TEST_SERVER_URL,
				4));
		dao.resetDatabase();

		int n = 5;

		for (int i = 0; i < n; i++) {
			Log aLog = new Log();
			aLog.setMachineId(Main.machineId);
			aLog.setTime(new Timestamp(System.currentTimeMillis()));
			aLog.setLog(Utils.loremIpsum(10000));
			aLog.setErrors("some exception");
			dao.saveLog(aLog);
		}

		log.info("saved the logs");

		List<Log> logs = dao.logs();
		assertTrue("retrieved all the logs", logs.size() == 5);
		assertEquals("retrieved exception", logs.get(0).getErrors(),
				"some exception");

		for (Log l : logs) {
			log.info(l + "\n");
		}
	}

}
