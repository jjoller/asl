#!/usr/bin/env bash

server=ubuntu@${1}

ssh ${server} sudo add-apt-repository ppa:webupd8team/java
ssh ${server} sudo apt-get update
ssh ${server} sudo apt-get upgrade
ssh ${server} sudo echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
ssh ${server} sudo apt-get install -y oracle-java8-installer ca-certificates

