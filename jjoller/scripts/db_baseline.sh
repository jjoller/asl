#!/bin/bash

# ./db_baseline.sh 16 8

dbserver='52.16.131.202'
server='52.30.140.17'
workerthreads=$1
connections=$2
jar='../ASL.jar'

echo "#### UPLOAD APPLICATION TO SERVERS ####"
ssh ubuntu@$server sudo killall -9 java
scp $jar ubuntu@$server:

echo "#### RESET DB ####"
java -jar $jar dbreset $dbserver

echo "#### START SERVER ####"
ssh ubuntu@$server java -jar ASL.jar dbbaseline $dbserver $workerthreads $connections &

