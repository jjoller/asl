#!/bin/bash

# ./run_experiment.sh 4 8 2000 200 200 randomized100

dbserver='52.30.246.175'
workerthreads=$1
connections=$2
runtime=$3
msglength=$4
numclients=$5
jar='../ASL.jar'
workload=$6
servermode=$7

declare -a clients=(52.31.0.255)
declare -a servers=(52.30.243.137)

for i in ${mwservers[@]}; do
	echo $mwservers[@]
done

echo "#### UPLOAD APPLICATION TO MW SERVERS AND CLIENTS ####"
for i in "${servers[@]}"
do
	ssh ubuntu@$i sudo killall -9 java
	scp $jar ubuntu@$i:
done
for i in "${clients[@]}"
do
	ssh ubuntu@$i sudo killall -9 java
	scp $jar ubuntu@$i:
done

echo "#### RESET DB ####"
java -jar $jar dbreset $dbserver

echo "#### START SERVERS AND CLIENTS ####"
index=0
for i in "${servers[@]}"
do
	echo "$index"
	echo ${clients[$index]}
	ssh ubuntu@$i java -jar ASL.jar $servermode $dbserver $workerthreads $connections $runtime &
	sleep 1
	ssh ubuntu@${clients[$index]} java -jar ASL.jar client $numclients ${servers[$index]} $msglength $dbserver $workload &
	index=$index+1
done

# wait for the clients to terminate and writing the logs
sleep $runtime
sleep 5

echo "#### Database status ####"
java -jar $jar dbstatus $dbserver

echo "#### DOWNLOAD LOGS ####"
# write the logs to file
java -jar $jar logs $dbserver $workerthreads-$connections-$msglength-$numclients-$workload


